package server_java;

import client_java.GameGUI;

import java.io.IOException;
import java.sql.SQLException;

/*
This class will be the basis of the threads that we use
- It will check if a game has enough players to start
- It will check if a game will start
- It will check if someone has already won a round
- It will check if someone has already
 */

public class WGame extends Thread {
    int gameId;
    boolean isGameOver = false;

    public WGame(int gameID) {
        this.gameId = gameID;
    }

    //    public static void main(String[] args) {
//    ///    WGame thread = new WGame();
//    ///    thread.start();
//    }
    public void run() {
        try {
            JavaDB javaDB = new JavaDB();
            // Thread handles the generation of letters
            MyRunnable myRunnable = new MyRunnable();
            Thread thread = new Thread(myRunnable);
            thread.start();
            System.out.println("[Server]Thread handling gameId: " + gameId);
            //10 sec count
            sleep(10000);
            //if there are at least 2 players in the game then it starts

            if (JavaDB.checkNumPlayers(gameId)) {
                JavaDB.updateGameStatusI(gameId);
                //loop keeps going on and on untill someone wins
                while (!isGameOver) {
                    JavaDB.processWinnerOfGame(gameId);
                    sleep(10000);
                    isGameOver = JavaDB.checkIfGameIsOver(gameId);
                    System.out.println("Round has ended");

                    if (JavaDB.checkIfThereIsEnoughPlayer(gameId) == false){
                        System.out.println("There is not enough players for GameId: " + gameId + " game is being dissolved");
                        isGameOver = true;
                        break;
                    }
                }
                //drop game objects
                JavaDB.dropGameTables(gameId);
                //game is finished
                JavaDB.updateGameStatusF(gameId);
            } else {
                //TODO Destroy game objects and set the game as "Disolved" from the games table
                //drop game objects
                JavaDB.dropGameTables(gameId);
                //game never started
                JavaDB.updateGameStatusD(gameId);
            }
            System.out.println("[SERVER] Thread for gameID: " + gameId + " has ended!");

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //Makes a new set of letters for every round. Letters will have to be made in advance
    //because it takes a while to make a set of letters roughly a few seconds but still very long
    public class MyRunnable implements Runnable {
        @Override
        public void run() {

            while (!isGameOver) {
                try {
                    JavaDB.generateLetters();
                    System.out.println("[Server]Letters generated for gameID: " + gameId);
                    sleep(7500);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }

    }
}

