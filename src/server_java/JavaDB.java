package server_java;

import WordyApp.GameStatus;
import WordyApp.LogInStatus;
import WordyApp.TopFivePlayers;
import WordyApp.TopFiveWords;
import common.WordyGame;
import server_java.CommonSql.Games;
import server_java.CommonSql.MatchInputs;
import server_java.game.GameRoom;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import static common.WordyGame.*;

/**
 * This class is used for setting up a connection and accessing the database.
 */
public class JavaDB {
    private static Connection con;

    /**
     * Establishes a connection to a MySQL database.
     *
     * @throws SQLException           If there is an error connecting to the database.
     * @throws ClassNotFoundException If the MySQL JDBC driver is not found.
     */
    public static void connect() throws SQLException, ClassNotFoundException {
        // Load the JDBC driver for your database
        Class.forName("com.mysql.cj.jdbc.Driver");
        // Set up the database connection (change niyo nalang yung sakanyo hehe)
        //            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/wordy_database?user=root&password");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/wordy_database?user=root&password");

    }

    /**
     * Checks if the account entered is valid or not.
     *
     * @param userName Username of the account entered.
     * @param passWord Password of the account entered.
     * @return If the account is valid.
     */
    public static boolean isUserValid(String userName, String passWord) {
        try {
            connect();
            PreparedStatement stmt = con.prepareStatement("SELECT COUNT(*) FROM user WHERE username = ? AND password = ?");
            System.out.println("The user inputted: " + userName + passWord);
            stmt.setString(1, userName);
            stmt.setString(2, passWord);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int count = rs.getInt(1); // get count of matches

           /* count = 0 (if user input does not match with data in db)
              count = 1 (if user input matches with data in db
            */

            if (count > 0) {
                return true; // user is logged in!
            }
            return false;

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkIfUserHasWrongPass(String userName, String passWord) {
        try {
            connect();
            PreparedStatement stmt = con.prepareStatement("SELECT password FROM user WHERE username = ? ");
            System.out.println("The user having: " + userName + passWord);
            stmt.setString(1, userName);
            ResultSet rs = stmt.executeQuery();

            // Check if any rows are returned
            if (rs.next()) {
                String storedPassword = rs.getString("password");
                if (!passWord.equals(storedPassword)) {
                    // User with provided username but wrong password
                    return true;
                }
            }

            // No user with matching username and 'Online' status found, or password is correct
            return false;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }


    public static boolean checkIfUserIsOnline(String userName, String passWord) {
        try {
            connect();
            PreparedStatement stmt = con.prepareStatement("SELECT status FROM user WHERE username = ? AND password = ? AND status = 'online'");
            System.out.println("The user having: " + userName + passWord);
            stmt.setString(1, userName);
            stmt.setString(2, passWord);
            ResultSet rs = stmt.executeQuery();

            // Check if any rows are returned
            if (rs.next()) {
                // User with provided username, password, and 'Online' status exists
                return true;
            }

            // No user with matching credentials and 'Online' status found
            return false;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Method to input guesses of the users
     * Wipe all the data after every round
     * Reyes
     */

    public static void wordStorage(String word) {
        try {
            PreparedStatement stmt = con.prepareStatement("INSERT INTO"); //what is data type for word input of user
            stmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Unable to store data.");
        }
    }

    /**
     * Match Winner Method
     * When a player joins you input their name round winner "default winner as no"
     * after every 10 seconds the same users would get inputted again but the round is incremented
     * Reyes
     */
    public static void matchWinner(String username) {
        try {
            PreparedStatement stmt = con.prepareStatement("SELECT from match where user = '" + username + "'");
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Displays to total wins of a user.
     *
     * @param userName username of the account.
     * @return The number of wins of an account.
     */

    public static int showTotalWins(String userName) {
        int totalWins = 0;
        try {
            connect();
            PreparedStatement stmt = con.prepareStatement("SELECT totalWin FROM users WHERE userName = ? ");
            stmt.setString(1, userName);

            ResultSet rs = stmt.executeQuery();
            rs.next();

            totalWins = rs.getInt(1);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return totalWins;
    }

    public static void updateStatus(boolean isOnline, String userName) {
        try {
            connect();
            String updateStatusQuery = "UPDATE users SET status = ? WHERE username = ?";
            PreparedStatement stmt = con.prepareStatement(updateStatusQuery);
            stmt.setString(1, isOnline ? "online" : "offline");

            stmt.setString(2, userName);
            stmt.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds 1 to the total win of a user and prints the top 5 users with the most total wins in descending order.
     */
    public static void updateTotalWins(String username) {
        try {
            connect();
            PreparedStatement stmt = con.prepareStatement("UPDATE user SET totalWin = totalWin + 1 WHERE userName = ? ");
            stmt.setString(1, username);
            stmt.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static TopFivePlayers[] getTopPlayers() {
        try {
            connect();

            // retrieve the top 5 users with the most total wins in descending order
            PreparedStatement stmt = con.prepareStatement("SELECT userName, totalWin, DENSE_RANK() OVER (ORDER BY totalWin DESC) AS \"rank\"\n" +
                    "FROM users\n" +
                    "ORDER BY totalWin DESC\n" +
                    "LIMIT 5;");
            ResultSet rs = stmt.executeQuery();
            TopFivePlayers[] topPlayers = new TopFivePlayers[5];


            int count = 0;
            while (rs.next()) {
                String username = rs.getString("userName");
                int totalWins = rs.getInt("totalWin");
                int rank = rs.getInt("rank");
                System.out.println(rank + " " + username + " " + totalWins);
                topPlayers[count] = new TopFivePlayers(rank, username, totalWins);
                count++;
            }

            /*
                for (TopFivePlayers t : topPlayers) {
                System.out.println("User: " + t.username + " WinAmt: " + t.winAmt);
            }
             */


            return topPlayers;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return new TopFivePlayers[0]; // Return an empty array in case of an exception
        }
    }

    public static TopFiveWords[] getTopFiveWords() {
        try {
            connect();
            // retrieve the top 5 words in descending order
            PreparedStatement stmt = con.prepareStatement("SELECT guessWord, userName, DENSE_RANK() OVER (ORDER BY LENGTH(guessWord) DESC) AS \"rank\"\n" +
                    "FROM guesses\n" +
                    "ORDER BY LENGTH(guessWord) DESC, guessWord DESC\n" +
                    "LIMIT 5;");

            ResultSet rs = stmt.executeQuery();


            TopFiveWords[] topWords = new TopFiveWords[5];
            int count = 0;
            while (rs.next()) {
                String username = rs.getString("userName");
                String longestWord = rs.getString("guessWord");
                int rank = rs.getInt("rank");
                System.out.println(rank + " " + username + " " + longestWord);
                /*
                  if (resultSetSize > 0 && username != null && longestWord != null){
                    topWords[count] = new TopFiveWords(rank,username, longestWord);
                }else {
                    topWords[count] = new TopFiveWords(0,"N/A", "N/A");
                }
                 */

                topWords[count] = new TopFiveWords(rank, username, longestWord);
                count++;

            }
            System.out.println("Code reached here");
            int resultSetSize = getResultSetSize(rs);
            System.out.println("Result size: " + resultSetSize);

            /*
                      for (TopFiveWords t : topWords) {
                System.out.println("Rank: " + t.rank + "User: " + t.username + " LongestWord: " + t.word);
            }
             */


            return topWords;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return new TopFiveWords[0]; // Return an empty array in case of an exception
        }
    }

    public static int getResultSetSize(ResultSet rs) throws SQLException {
        int size = 0;
        while (rs.next()) {
            size++;
        }
        return size;
    }

    //String
    public static void createNewGame(String userName) throws SQLException, ClassNotFoundException {
        connect();
        //get the number of games first then concatenate it for the name of the match
        int numOfGames;
        PreparedStatement stmt = con.prepareStatement("SELECT COUNT(*) FROM games;");
        ResultSet rs = stmt.executeQuery();
        rs.next();
        numOfGames = rs.getInt(1) + 1;


        System.out.println("NewGame Id will be: " + numOfGames);
        String newGame;
        String statement;
        //=======================================

        //create a new table for the inputs of the players ==========================
        newGame = "match" + numOfGames + "inputs";
        statement = "CREATE TABLE " + newGame + " (\n" + "id INT,\n" + "round INT,\n" + "userName VARCHAR(50),\n" + "input VARCHAR(500)\n" + ");\n";
        PreparedStatement stmt2 = con.prepareStatement(statement);
        stmt2.execute();

        //create a new table for the letters that the players will use==========================
        newGame = "match" + numOfGames + "letters";
        statement = "CREATE TABLE " + newGame + " (\n" + "id INT,\n" + "round INT,\n" + "letters VARCHAR(50)\n" + ");\n";
        PreparedStatement stmt3 = con.prepareStatement(statement);
        stmt3.execute();

        // add the game in our list of games
        PreparedStatement stmt4 = con.prepareStatement("INSERT INTO games VALUES (?,?,?,?,?,?,?) ");
        stmt4.setInt(1, numOfGames);
        stmt4.setString(2, userName);
        stmt4.setString(3, "Waiting");
        //it's one because it's the player that initialized it is only one player
        //NOTE SET THIS TO ZERO WHEN NOT TESTING
        //NOTE SET THIS TO ZERO WHEN NOT TESTING
        //NOTE SET THIS TO ZERO WHEN NOT TESTING
        //NOTE SET THIS TO ZERO WHEN NOT TESTING this is the number of players
        stmt4.setInt(4, 0);
        //winners
        stmt4.setString(5, "N/A");
        //userLongWord
        stmt4.setString(6, "N/A");
        //longestWord
        stmt4.setString(7, "N/A");
        stmt4.execute();
        //Waiting/In game/Dissolved/Finished
    }

    public static boolean checkNumPlayers(int gameId) throws SQLException, ClassNotFoundException {
        connect();
        Boolean gameStart = false;
        String statement = "SELECT numPlayers FROM games WHERE gameId =" + gameId + ";";
        PreparedStatement stmt = con.prepareStatement(statement);
        ResultSet rs = stmt.executeQuery();
        rs.next();

        int numOfPlayers = rs.getInt(1);
        System.out.println(numOfPlayers);
        if (numOfPlayers > 1) {
            //this is the part where we start the game
            statement = "UPDATE games SET status = 'In GameRoom' WHERE gameId = " + gameStart + "; ";
            stmt = con.prepareStatement(statement);
            stmt.execute();

            gameStart = true;
            //TODO update the game status to 'IN GAME'


        } else if (numOfPlayers == 1) {
            //this is the part where we drop the tables
            gameStart = false;
        }
        return gameStart;

    }

    //generate the letters per round for a game
    public static String generateLetters() throws IOException, SQLException, ClassNotFoundException {
        WordyGame wg = new WordyGame();
        ArrayList<String> words = wg.loadWordsFromFile();
        Random random = new Random();
        int vowelAmt = wg.vowelCount - rand.nextInt(3);
        System.out.println("Amount of vowels: " + vowelAmt);

        //TODO: SQLConnection to Database MAKE SURE THE LETTERS GIVEN WILL BE ABLE TO FORM A WORD !!!
        StringBuilder vowelLetters = generateRandomLetters(vowels, vowelAmt, wg.rand);
        StringBuilder consonantLetters = generateRandomLetters(wg.consonants, wg.consonantCount, wg.rand);
        StringBuilder letters = wg.combineLetters(vowelLetters, consonantLetters);

        while (!canFormWord(words, letters)) {
            vowelLetters = generateRandomLetters(vowels, vowelAmt, rand);
            consonantLetters = generateRandomLetters(consonants, consonantCount, rand);
            letters = combineLetters(vowelLetters, consonantLetters);
        }

        int count = countValidWords(words, letters);
        // System.out.println("Available Words that can be formed: " + count);
        return letters.toString();
    }

    //We won't need to concate it anymore just roll over the list instead. Much easier.
    public static synchronized void userInput(int gameId, int round, String userName, String guess) throws SQLException, ClassNotFoundException {
        String tableName = "match" + gameId + "inputs";
        connect();

        //count the number of rows
        String statement = "SELECT COUNT(*) FROM " + tableName;
        PreparedStatement stmt = con.prepareStatement(statement);
        ResultSet rs = stmt.executeQuery();
        rs.next();
        //+1 because this will be the next row
        int newId = rs.getInt(1) + 1;
        stmt.execute();

        statement = "INSERT INTO " + tableName + " (id,round,userName,input) VALUES (?,?,?,?)";
        PreparedStatement stmt1 = con.prepareStatement(statement);
        stmt1.setInt(1, newId);
        stmt1.setInt(2, round);
        stmt1.setString(3, userName);
        stmt1.setString(4, guess);
        stmt1.execute();

    }

    //returns the id of a open game
    public static int searchOpenGame() throws SQLException {
        String statement = "SELECT gameID FROM wordy_database.games WHERE status = ?";
        PreparedStatement stmt1 = con.prepareStatement(statement);
        stmt1.setString(1, "Waiting");
        ResultSet rs = stmt1.executeQuery();

        if (!rs.next()) {
            System.out.println("[SERVER] No game can be found!");
            //0 is returned if no game can be found NULL cannot be passed back in corba
            return 0;
        } else {
            int openGameId = rs.getInt(1);
            //TODO update the number of players in said game
            statement = "UPDATE games\n" + "SET numPlayers = numPlayers + 1\n" + "WHERE gameId = ?;";
            PreparedStatement stmt2 = con.prepareStatement(statement);
            stmt2.setInt(1, openGameId);
            stmt2.execute();
            System.out.println("[SERVER] A Player has joined a game with the Id: " + openGameId);
            return openGameId;
        }
    }


    //process the winner from matchXinputs
    //we do this by checking each round
    //then adding the winner for that specific round in to matchXwinners
    //so we should procress it each round??
    public static void processWinnerOfGame(int gameId) throws SQLException, ClassNotFoundException {
        //TODO algorithm to determine the winner of a round
        String tableName = "match" + gameId + "inputs";

        connect();

        //get the number of rounds
        String statement = "  SELECT COUNT(DISTINCT round) AS numRounds FROM " + tableName;
        PreparedStatement stmt1 = con.prepareStatement(statement);
        ResultSet rs = stmt1.executeQuery();
        rs.next();
        int numberOfRounds = rs.getInt(1);
        rs = null;

        System.out.println("[SERVER] GameID: " + gameId + " " + numberOfRounds + " rounds that has user inputs. Scanning for winners");
        if (numberOfRounds > 2) {

            String winner = null;
            String userLongWord;
            String longestWord;
            boolean aPlayerWon = false;

            //grab the whole table check for the longest word
            ArrayList<MatchInputs> matchResultList = new ArrayList<>();
            statement = "  SELECT * FROM " + tableName;
            PreparedStatement stmt2 = con.prepareStatement(statement);
            rs = stmt2.executeQuery();

            while (rs.next()) {
                MatchInputs m = new MatchInputs(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4));
                matchResultList.add(m);
            }
            rs = null;
            //    for (MatchInputs m : matchResultList) {
            //        System.out.println(m.getId() + " " + m.getRound() + " " + m.getUserName() + " " + m.getInput());
            //    }
            //clear the list after every query just to be safe
            matchResultList.clear();


            //query will get the winner for each round. If the longest words have duplicates then it ignores them.
            // do not foget "\n" apparently it becomes a syntax error if you don't
            statement = " SELECT id, round, userName, input\n" + "FROM (\n" + "  SELECT id, round, userName, input,\n" + "         RANK() OVER (PARTITION BY round ORDER BY LENGTH(input) DESC) AS inputRank,\n" + "         COUNT(*) OVER (PARTITION BY round, LENGTH(input)) AS inputCount\n" + "  FROM  " + tableName + "\n" + ") AS subquery\n" + "WHERE inputRank = 1\n" + "  AND inputCount = 1;";

            PreparedStatement stmt3 = con.prepareStatement(statement);
            rs = stmt3.executeQuery();

            //extract the winners of each round
            while (rs.next()) {
                MatchInputs m = new MatchInputs(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4));
                matchResultList.add(m);
            }
            for (MatchInputs m : matchResultList) {
                System.out.println(m.getId() + " " + m.getRound() + " " + m.getUserName() + " " + m.getInput());
            }
            rs = null;


            //UNABLE OT SUB FURTHER AS TO WHY THIS SECTION EXIST
            //use a hash so taht there would be no duplicate names
            Set<String> playerList = new HashSet<>();
            // Get the names of all players
            for (MatchInputs mI : matchResultList) {
                playerList.add(mI.getUserName());
            }

            // count the number of times a  player has won
            for (String p : playerList) {
                String curUserName = p; // The username you want to check

                int count = 0;
                //loop through the entire match result list !NOTE THIS LIST DOES NOT CONTAIN DUPES!!!
                for (int i = 0; i < matchResultList.size(); i++) {
                    //get the row of mi
                    MatchInputs matchInputs = matchResultList.get(i);
                    //if the name matched and it appeared we increment the # of times it appeared
                    if (matchInputs.getUserName().equals(curUserName)) {
                        count++;
                        //if a player has already won three times then we save his name
                        if (count == 3) {
                            System.out.println(curUserName + " appeared three times.");
                            //save the winner
                            winner = curUserName;
                            aPlayerWon = true;
                            break;
                        }
                    }
                }
            }
            //clear the list after every query just to be safe
            matchResultList.clear();

            //updates the longest word
            if (aPlayerWon) {
                //Get the longest word with their user who made the longest word
                statement = "SELECT userName, input\n" + "FROM " + tableName + " \n" + "WHERE LENGTH(input) = (\n" + "    SELECT MAX(LENGTH(input))\n" + "    FROM " + tableName + "\n" + ")\n" + "LIMIT 1;";
                PreparedStatement stmt4 = con.prepareStatement(statement);
                rs = stmt4.executeQuery();
                rs.next();
                userLongWord = rs.getString(1);
                longestWord = rs.getString(2);

                //update the games table with the winner
                System.out.println(userLongWord + " " + longestWord);
                statement = "UPDATE games SET status ='Finished' ,winners = '" + winner + "', userLongWord = '" + userLongWord + "', longestWord = '" + longestWord + "' WHERE gameId = " + gameId;
                PreparedStatement stmt5 = con.prepareStatement(statement);
                stmt5.execute();
            }


            // increment the total wins of the user who won
            JavaDB.updateTotalWins(winner);
        }
    }


    public static boolean checkIfGameIsOver(int gameId) throws SQLException {
        boolean isGameOver = false;
        String status;
        String statement = "SELECT status FROM games WHERE gameId = ? AND status = 'Finished';";
        PreparedStatement stmt = con.prepareStatement(statement);
        stmt.setInt(1, gameId);
        ResultSet rs = stmt.executeQuery();

        System.out.println("[SERVER] Checking if the game has ended");
        //if result set is empty
        if (!rs.next()) {
            System.out.println("[SERVER] No game can be found!");
        } else {
            System.out.println("[SERVER] GameID: " + gameId + " has ended!");
            status = rs.getString(1);
            System.out.println(status);
            if (status.equals("Finished")) {
                isGameOver = true;
            }
        }
        return isGameOver;
    }

    /**
     * Method to drop table after match end
     *
     * @param gameId Reyes
     */

    public static void dropGameTables(int gameId) {

        String tableName = "match" + gameId + "inputs";
        String tableName2 = "match" + gameId + "letters";
        try {
            System.out.println("GameID:" + gameId + " Has been dropped");
            PreparedStatement stmt = con.prepareStatement("DROP TABLE " + tableName);
            stmt.executeUpdate();
            PreparedStatement stmt2 = con.prepareStatement("DROP TABLE " + tableName2);
            stmt2.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Cannot drop table because: " + e);
        }
    }

    //update a games status to dissolve
    public static void updateGameStatusD(int gameId) {
        try {
            PreparedStatement stmt = con.prepareStatement("UPDATE games SET status = 'Dissolved' WHERE gameId = ?;");
            stmt.setInt(1, gameId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //finished
    public static void updateGameStatusF(int gameId) {
        try {
            PreparedStatement stmt = con.prepareStatement("UPDATE games SET status = 'Finished' WHERE gameId = ?;");
            stmt.setInt(1, gameId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //in game

    public static void updateGameStatusI(int gameId) {
        try {
            PreparedStatement stmt = con.prepareStatement("UPDATE games SET status = 'In game' WHERE gameId = ?;");
            stmt.setInt(1, gameId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Scanner kbd = new Scanner(System.in);
        System.out.println("enter id: ");
        int id = kbd.nextInt();
    }

    public static Boolean userGuessWithoutDupe(int gameId, int round, String userName, String guess) {
        try {
            // TODO a query to get the letters alotted to that specific round
            connect();
            System.out.println("The user: " + userName + " sent: " + guess + " to gameID: " + gameId);
            String tableName = "match" + gameId + "inputs";
            String tableName2 = "match" + gameId + "letters";

            String statement1 = "SELECT letters \n" + "FROM " + tableName2 + " \n" + "WHERE round = ? ;";
            PreparedStatement stmt1 = con.prepareStatement(statement1);
            stmt1.setInt(1, round);
            ResultSet rs1 = stmt1.executeQuery();

            String letters = null;
            if (rs1.next()) {
                letters = rs1.getString(1);
                //        System.out.println("[CLIENT] The letters from the round are: " + letters);
            } else {
                System.out.println("No letters found.");
            }

            System.out.println("The letters from the round are: " + letters);

            // TODO check if the word is valid

            Boolean isGuessRight = false;
            if (isValidWord(guess, letters)) {
                // TODO we use that to check if we can insert or update for later (This is done already)
                // CHECK IF THE INPUT TABLE IS EMPTY IF IT'S NOT THEN WE HAVE TO USER INSERT!!
                String statement2 = "SELECT COUNT(*) AS rowCount\n" + "FROM " + tableName + ";";
                PreparedStatement stmt2 = con.prepareStatement(statement2);
                ResultSet rs2 = stmt2.executeQuery();
                rs2.next();
                int id = rs2.getInt(1);
                System.out.println("The number of rows in the " + tableName + " is: " + id);
                // if the table is still empty

                if (id == 0) {
                    System.out.println("The word: " + guess + " has been inserted in the db");
                    String statement3 = "INSERT INTO " + tableName + "(id, round, userName, input)\n" + "VALUES (?,?,?,?);\n";
                    PreparedStatement stmt3 = con.prepareStatement(statement3);
                    stmt3.setInt(1, id + 1);
                    stmt3.setInt(2, round);
                    stmt3.setString(3, userName);
                    stmt3.setString(4, guess);
                    stmt3.execute();
                    // if the table is not empty
                } else if (id > 0) {
                    // we check if there is a word that has a similar length
                    String statement3 = "SELECT COUNT(*) AS word_count\n" + "FROM " + tableName + " \n" + "WHERE LENGTH(input) = LENGTH(?)\n" + "  AND round = ? \n" + "  AND userName = ? ;";
                    PreparedStatement stmt3 = con.prepareStatement(statement3);
                    stmt3.setString(1, guess);
                    stmt3.setInt(2, round);
                    stmt3.setString(3, userName);
                    ResultSet rs3 = stmt3.executeQuery();
                    rs3.next();

                    // word that has a matched length
                    int matchedLength = rs3.getInt(1);
                    System.out.println("Matched: " + matchedLength);

                    // if we found a word that has the same length then we just update it
                    if (matchedLength > 0) {
                        System.out.println("[Server] Inserting new input because there is a match");
                        // TODO update word with the same length
                        String statement4 = "UPDATE " + tableName + " \n" + "SET input = ? \n" + "WHERE LENGTH(input) = LENGTH(?)\n" + "  AND round = ? \n" + "  AND userName = ? ;";
                        PreparedStatement stmt4 = con.prepareStatement(statement4);
                        stmt4.setString(1, guess);
                        stmt4.setString(2, guess);
                        stmt4.setInt(3, round);
                        stmt4.setString(4, userName);
                        stmt4.execute();

                        // if no match was found
                    } else if (matchedLength <= 0) {
                        System.out.println("[Server] Inserting new input because there is no match");
                        // TODO insert because there is no word with the same length
                        String statement5 = "INSERT INTO " + tableName + " (id,round, userName, input)\n" + "VALUES (?,?,?,?);";
                        PreparedStatement stmt5 = con.prepareStatement(statement5);
                        stmt5.setInt(1, id + 1);
                        stmt5.setInt(2, round);
                        stmt5.setString(3, userName);
                        stmt5.setString(4, guess);
                        stmt5.execute();
                    }
                }

                isGuessRight = true;
                // count the number of rows
                // if zero then we insert
                // count the number of words that are the same length as x same round username etc
                // if there is a match update
                // if there is no match insert
            } else {
                isGuessRight = false;
                System.out.println("The word: " + guess + " is incorrect!");
            }
            return isGuessRight;

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    public static GameStatus checkMatchStatus(GameStatus gameStatus, int gameId) {
        try {
            connect();


            String statement1 = "SELECT status \n" + "FROM games \n" + "WHERE gameId = ? ;";

            PreparedStatement stmt1 = con.prepareStatement(statement1);
            stmt1.setInt(1, gameId);
            ResultSet rs1 = stmt1.executeQuery();

            System.out.println("Is there enough players in game: " + checkIfThereIsEnoughPlayer(gameId));
            if (rs1.next()) {
                String status = rs1.getString(1);
                //         System.out.println("[CLIENT] GameID: " + gameId + " Status: " + status);
                if (status.equalsIgnoreCase("Waiting")) {
                    gameStatus.hasGameStarted = false;
                } else if (status.equalsIgnoreCase("In game")) {
                    gameStatus.hasGameStarted = true;
                } else if (status.equalsIgnoreCase("Dissolved")) {
                    gameStatus.hasGameDissolved = true;
                } else if (status.equalsIgnoreCase("Finished")) {
                    if (checkIfThereIsEnoughPlayer(gameId) == false) {
                        System.out.println("GameId: " + gameId + "Has only 1 player left!");
                        gameStatus.lastPlayer = true;
                    } else {
                        gameStatus.hasGameEnded = true;
                        //TODO WE NEED A QUERY TO GET THE NAME OF THE WINNER
                        gameStatus.nameOfWinner = getWinnerOfGame(gameId);
                        //TODO query to get the number of players
                    }

                }
            } else {
                // Handle the case when no rows are returned in the ResultSet
                // You might want to set appropriate default values or throw an exception
            }

            return gameStatus;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    // Method will check if there is enough players
    static boolean checkIfThereIsEnoughPlayer(int gameId) throws SQLException {
        String statement1 = "SELECT numPlayers FROM games WHERE gameId = ?";
        PreparedStatement stmt = con.prepareStatement(statement1);
        stmt.setInt(1, gameId);
        ResultSet rs = stmt.executeQuery();

        int numOfPlayers = 0;
        if (rs.next()) {
            numOfPlayers = rs.getInt("numPlayers");
            System.out.println("[CLIENT] Requesting the number of players left: " + numOfPlayers + " GameId: " + gameId);
        } else {
            System.out.println("[SERVER] Could not find the number of players!");
        }

        // If there's more than one player, the game won't dissolve
        if (numOfPlayers > 1) {
            return true;
        } else if (numOfPlayers <= 1) {
            return false;
        } else {
            return true;
        }

    }


    //TODO METHOD THAT WILL QUERY THE NAME OF THE WINNER
    private static String getWinnerOfGame(int gameId) throws SQLException {
        //TODO query over here

        String statement1 = "SELECT winners FROM games WHERE gameId = ?;";
        PreparedStatement stmt = con.prepareStatement(statement1);
        stmt.setInt(1, gameId);
        ResultSet rs = stmt.executeQuery();

        if (rs.next()) {
            String winner = rs.getString(1);
            System.out.println("[CLIENT] Requesting winner of gameId: " + gameId + "Winner: " + winner);
            return winner;
        } else {
            return "Query could not find winner";
        }
    }

    public static ArrayList<String> loadWordsFromFile() throws IOException {
        ArrayList<String> words = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader("resources/games/words.txt"));
        String line;
        while ((line = reader.readLine()) != null) {
            words.add(line.trim().toLowerCase());
        }
        reader.close();
        return words;
    }

    /*
     public static void addNewGameToGamesTable(GameRoom g) throws SQLException, ClassNotFoundException {
        Games game = new Games(g.getGameId(), g.getInitializer(), g.getStatus(), g.getNumPlayers(), "", "", "");
        connect();

        String query = "INSERT INTO games (gameId, initializer, status, numPlayers, winners, userLongWord, longestWord) " + "VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement stmt1 = con.prepareStatement(query);
        stmt1.setInt(1, game.getGameId());
        stmt1.setString(2, game.getInitializer());
        stmt1.setString(3, game.getStatus());
        stmt1.setInt(4, game.getNumPlayers());
        stmt1.setString(5, game.getWinners());
        stmt1.setString(6, game.getUserLongWord());
        stmt1.setString(7, game.getLongestWord());
        stmt1.executeUpdate();
        stmt1.close();
    }
     */
    public static void addWordToGuessTable(int gameId, String guess, String userName) throws SQLException, ClassNotFoundException {
        connect();
        String query = "INSERT INTO guesses (gameId, guessWord, userName) VALUES (?,?,?)";
        PreparedStatement stmt1 = con.prepareStatement(query);
        stmt1.setInt(1, gameId);
        stmt1.setString(2, guess);
        stmt1.setString(3, userName);
        stmt1.executeUpdate();
        stmt1.close();
    }

    public static boolean isWordOnList(String guess) throws IOException {
        //I added this loop because idk how on earth the words wereb eing checkedi n first place ??!?!?!
        ArrayList<String> words = loadWordsFromFile();
        for (String word : words) {
            if (word.equals(guess)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidWord(String guess, String letters) throws IOException {

        if (guess == null || guess.isEmpty()) {
            return false;
        }

        Map<Character, Integer> letterCounts = new HashMap<>();
        for (char c : letters.toCharArray()) {
            letterCounts.put(c, letterCounts.getOrDefault(c, 0) + 1);
        }

        for (char c : guess.toCharArray()) {
            if (!letterCounts.containsKey(c) || letterCounts.get(c) <= 0) {
                return false;
            }
            letterCounts.put(c, letterCounts.get(c) - 1);
        }


        return true;
    }


    public static synchronized String getLettersFromGame(int gameId, int round) throws SQLException, ClassNotFoundException {
        connect();

        String tableName = "match" + gameId + "inputs";
        String tableName2 = "match" + gameId + "letters";

        String statement1 = "SELECT letters FROM " + tableName2 + " WHERE round = ?;";
        PreparedStatement stmt = con.prepareStatement(statement1);
        stmt.setInt(1, round);
        ResultSet rs = stmt.executeQuery();


        if (rs.next()) {
            String letters = rs.getString(1);
            System.out.println("[CLIENT] Requesting Letters: " + letters + " From GameID: " + gameId + " Round: " + round);
            return letters;
        } else {
            return "No more characters!";
        }
    }

    public static String getConsonants(String letters) {
        String consonants = letters.substring(0, 7);
        return consonants;
    }

    public static String getVowels(String letters) {
        String vowels = letters.substring(8, 17);
        return vowels;
    }

    public static void setDefaultWinner(String userName, int gameId) {

        //add the last players in the game to be the winner
        String query = "UPDATE games SET winners = '" + userName + "' WHERE gameId = " + gameId + ";";
        try {
            PreparedStatement ps = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // increment his win by 1
        setDefaultWinAmt(gameId, userName);
    }

    public static void setDefaultWinAmt(int gameId, String userName) {
        //TODO query that will subtract the number of players from the games table
        String query = "UPDATE `users` SET `totalWin`= totalWin + 1 WHERE userName = '" + userName + "' ;";
        try {
            PreparedStatement ps = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("User: " + userName + "TotalWins has been updated due to dissolved game");
        //update numPlayers From games to - 1

    }

    public static void subtractNumOfPlayers(int gameId) {
        //TODO query that will subtract the number of players from the games table
        String query = "UPDATE `games` SET `numPlayers`= numPLayers - 1 WHERE gameId = " + gameId + "";
        try {
            PreparedStatement ps = con.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //update numPlayers From games to - 1
    }

    public static LogInStatus logIn(String userName, String passWord) throws SQLException, ClassNotFoundException {
        LogInStatus logInStatus = new LogInStatus();
        //TODO FIRST QUERY WILL CHECCK IF THE USER EXIST
        logInStatus.doesUserExist = doesUserExist(userName);
        //TODO SECOND WILL CHECK IF THE PASS WORD IS CORRECT FOR THAT USER
        logInStatus.isPassWordCorrect = isPassWordCorrect(userName, passWord);
        //TODO THRID QUERY WILL CHECK IF THE USER IS ONLINE
        logInStatus.isUserOnline = isUserOnline(userName);
        //TODO query that will get the id of the user
        logInStatus.id = getUserId(userName);

        //TODO THE STATUS OF A USER TO ONLINE
        if (logInStatus.doesUserExist == true && logInStatus.isPassWordCorrect == true && logInStatus.isUserOnline == false) {
            updateStatus(true, userName);
        }

        return logInStatus;
    }


    private static int getUserId(String userName) {
        int userId = 0;
        try {
            connect();
            String query = "SELECT userId\n" + "FROM users \n" + "WHERE userName = ?;";
            PreparedStatement stmt1 = con.prepareStatement(query);
            stmt1.setString(1, userName);
            ResultSet rs = stmt1.executeQuery();

            if (rs.next()) { // Check if the result set has any rows
                userId = rs.getInt(1);
            } else {
                System.out.println("cannot get id, user does not exist");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        return userId;
    }


    public static boolean doesUserExist(String userName) throws SQLException, ClassNotFoundException {
        connect();
        String query = "SELECT userName FROM users WHERE BINARY userName = ?";
        PreparedStatement stmt1 = con.prepareStatement(query);
        stmt1.setString(1, userName);
        ResultSet rs = stmt1.executeQuery();
        //if the rs is empty
        if (!rs.next()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isPassWordCorrect(String userName, String passWord) throws SQLException, ClassNotFoundException {
        connect();
        String query = "SELECT * FROM users WHERE userName = ? AND password = ?";
        PreparedStatement stmt1 = con.prepareStatement(query);
        stmt1.setString(1, userName);
        stmt1.setString(2, passWord);
        ResultSet rs = stmt1.executeQuery();
        //if the rs is empty
        if (!rs.next()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isUserOnline(String userName) throws SQLException, ClassNotFoundException {
        connect();
        String query = "SELECT * FROM users WHERE userName = ? AND status = ?";
        PreparedStatement stmt1 = con.prepareStatement(query);
        stmt1.setString(1, userName);
        stmt1.setString(2, "offline");
        ResultSet rs = stmt1.executeQuery();
        //if the rs is empty
        if (!rs.next()) {
            return true;
        } else {
            return false;
        }

    }


    //returns hte numbert of columns. We need this so taht we won't mess up their id's
    public static int getGamesNumberOfRows() throws SQLException {
        int numOfRows = 0;
        String query = "SELECT COUNT(*) FROM games";
        PreparedStatement stmt1 = con.prepareStatement(query);
        ResultSet rs = stmt1.executeQuery();
        //if the rs is empty
        rs.next();
        //we need to increment it by one because it's a new game
        numOfRows = rs.getInt(1) + 1;
        return numOfRows;
    }


    public static void addNewGameToGamesTable(GameRoom g) throws SQLException, ClassNotFoundException {
        Games game = new Games(g.getGameId(), g.getInitializer(), g.getStatus(), g.getNumPlayers(), "", "", "");
        connect();

        String query = "INSERT INTO games (gameId, initializer, status, numPlayers, winners, userLongWord, longestWord) " + "VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement stmt1 = con.prepareStatement(query);
        stmt1.setInt(1, game.getGameId());
        stmt1.setString(2, game.getInitializer());
        stmt1.setString(3, game.getStatus());
        stmt1.setInt(4, game.getNumPlayers());
        stmt1.setString(5, game.getWinners());
        stmt1.setString(6, game.getUserLongWord());
        stmt1.setString(7, game.getLongestWord());
        stmt1.executeUpdate();
        stmt1.close();
    }

    public static void updateDissolvedGameToGamesTable(GameRoom g) throws SQLException, ClassNotFoundException {
        Games game = new Games(g.getGameId(), g.getInitializer(), g.getStatus(), g.getNumPlayers(), "", "", "");
        connect();
        String query = "UPDATE games SET initializer = ?, status = ?, numPlayers = ?, winners = ?, userLongWord = ?, longestWord = ? " + "WHERE gameId = ?";
        PreparedStatement stmt1 = con.prepareStatement(query);
        stmt1.setString(1, game.getInitializer());
        stmt1.setString(2, game.getStatus());
        stmt1.setInt(3, game.getNumPlayers());
        stmt1.setString(4, game.getWinners());
        stmt1.setString(5, game.getUserLongWord());
        stmt1.setString(6, game.getLongestWord());
        stmt1.setInt(7, game.getGameId());
        stmt1.executeUpdate();
        stmt1.close();
    }


    public static void updateFinishedGameToGamesTable(GameRoom g) throws SQLException, ClassNotFoundException {
        Games game = new Games(g.getGameId(), g.getInitializer(), g.getStatus(), g.getNumPlayers(), g.getWinnner(), g.getUserLongestWord(), g.getLongestWord());
        connect();
        String query = "UPDATE games SET initializer = ?, status = ?, numPlayers = ?, winners = ?, userLongWord = ?, longestWord = ? " + "WHERE gameId = ?";
        PreparedStatement stmt1 = con.prepareStatement(query);
        stmt1.setString(1, game.getInitializer());
        stmt1.setString(2, game.getStatus());
        stmt1.setInt(3, game.getNumPlayers());
        stmt1.setString(4, game.getWinners());
        stmt1.setString(5, game.getUserLongWord());
        stmt1.setString(6, game.getLongestWord());
        stmt1.setInt(7, game.getGameId());
        stmt1.executeUpdate();
        stmt1.close();
    }


    public static void incrementTotalWinByUsername(String username) throws SQLException, ClassNotFoundException {
        connect();

        String query = "UPDATE users SET totalWin = totalWin + 1 WHERE userName = ?";
        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setString(1, username);

        stmt.executeUpdate();
        stmt.close();
    }

    public static void addToJoinedGames(int userId, int gameId) throws SQLException, ClassNotFoundException {

        String tableName = "joined_games";
        connect();

        // Count the number of rows
        String countStatement = "SELECT COUNT(*) FROM " + tableName;
        PreparedStatement countStmt = con.prepareStatement(countStatement);
        ResultSet countRs = countStmt.executeQuery();
        countRs.next();
        // +1 because this will be the next row
        int newId = countRs.getInt(1) + 1;

        // Insert new row
        String insertStatement = "INSERT INTO " + tableName + " (id, userId, gameId) VALUES (?, ?, ?)";
        PreparedStatement insertStmt = con.prepareStatement(insertStatement);
        insertStmt.setInt(1, newId);
        insertStmt.setInt(2, userId);
        insertStmt.setInt(3, gameId);
        insertStmt.execute();
    }
}