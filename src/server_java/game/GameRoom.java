package server_java.game;

import server_java.CommonSql.MatchInputs;
import server_java.CommonSql.MatchLetters;
import server_java.CommonSql.MatchPlayers;
import server_java.CommonSql.RoundWinner;

import java.rmi.MarshalException;
import java.util.ArrayList;

public class GameRoom {
    int gameId;
    int numPlayers = 0;

    int timeBeforeGameStarts = 10;
    int timeBeforeNextRound = 20;

    int round = 1;

    public boolean isKeepGeneratingLetters() {
        return keepGeneratingLetters;
    }

    public void setKeepGeneratingLetters(boolean keepGeneratingLetters) {
        this.keepGeneratingLetters = keepGeneratingLetters;
    }

    boolean keepGeneratingLetters = true;

    String status;
    String initializer;
    String winnner;
    String userLongestWord;
    String longestWord;
    ArrayList <MatchPlayers> matchPlayersArrayList = new ArrayList<>();

    public ArrayList<MatchPlayers> getRealTimeMatchPlayersArrayList() {
        return realTimeMatchPlayersArrayList;
    }

    public void setRealTimeMatchPlayersArrayList(ArrayList<MatchPlayers> realTimeMatchPlayersArrayList) {
        this.realTimeMatchPlayersArrayList = realTimeMatchPlayersArrayList;
    }

    ArrayList <MatchPlayers> realTimeMatchPlayersArrayList = new ArrayList<>();

    public ArrayList <MatchInputs> listOfWinnersPerRound = new ArrayList<>();
    ArrayList <MatchInputs> matchInputsArrayList = new ArrayList<>();
    ArrayList <MatchLetters> matchLettersArrayList = new ArrayList<>();


    public String checkWinnerOfRound(int round){
        String winnerOfRound = "N/A";
        for (MatchInputs m: listOfWinnersPerRound){
            if (m.getRound() == round){
                winnerOfRound = m.getUserName();
            }
        }
        return winnerOfRound;
    }

    //update the list of winners per round
    public void updateWinnersPerRound(ArrayList <MatchInputs> matchLettersArrayList){
        ArrayList<MatchInputs> longestInputsByRound = new ArrayList<>();
        String winner = "N/A";

        for (MatchInputs input : matchInputsArrayList) {
            boolean hasIdenticalInput = false;

            // Check if the current input has an identical input among the others in the same round
            for (MatchInputs otherInput : matchInputsArrayList) {
                //if same round
                if (input.getRound() == otherInput.getRound() &&
                        //if not the same username
                        !input.getUserName().equals(otherInput.getUserName()) &&
                        //if is a identical
                        input.getInput().equals(otherInput.getInput())) {
                    hasIdenticalInput = true;
                    break;
                }
            }

            // If the input does not have an identical input check if it is the longest input for the current round
            if (!hasIdenticalInput) {
                int round = input.getRound();
                boolean isLongest = true;

                // Check if there is a longer input already stored for the current round
                for (MatchInputs longestInput : longestInputsByRound) {
                    if (longestInput.getRound() == round &&
                            longestInput.getInput().length() >= input.getInput().length()) {
                        isLongest = false;
                        break;
                    }
                }

                // If the input is the longest remove any existing inputs for the same round and add the current input
                if (isLongest) {
                    //only have the longest input per round NOTE m is tthe represnetatioin of MatchInputs
                    //ensures we keep only the lognest word
                    longestInputsByRound.removeIf(m -> m.getRound() == round);
                    longestInputsByRound.add(input);
                }

            }
        }

        this.listOfWinnersPerRound = longestInputsByRound;
    }
    public String getLetters(int round){
        for (MatchLetters m: matchLettersArrayList){
            if (m.getRound() == round){
                return m.getLetters();
            }
        }
        return "There are no more letters available";
    }


    public void addNumPlayer(){
        numPlayers++;
    }

    public void addMatchPlayer(MatchPlayers matchPlayer) {
        matchPlayersArrayList.add(matchPlayer);
    }

    public void removeMatchPlayer(MatchPlayers matchPlayer) {
        matchPlayersArrayList.remove(matchPlayer);
    }

    public void removeRealTimeMatchPlayer(MatchPlayers matchPlayer) {
        realTimeMatchPlayersArrayList.remove(matchPlayer);
    }

    public void addMatchInput(MatchInputs matchInput) {
        matchInputsArrayList.add(matchInput);
    }

    public void removeMatchInput(MatchInputs matchInput) {
        matchInputsArrayList.remove(matchInput);
    }

    public void addMatchLetter(MatchLetters matchLetter) {
        matchLettersArrayList.add(matchLetter);
    }

    public void removeMatchLetter(MatchLetters matchLetter) {
        matchLettersArrayList.remove(matchLetter);
    }


    public ArrayList<MatchPlayers> getMatchPlayersArrayList() {
        return matchPlayersArrayList;
    }

    public ArrayList<MatchInputs> getMatchInputsArrayList() {
        return matchInputsArrayList;
    }
    public ArrayList<MatchLetters> getMatchLettersArrayList() {
        return matchLettersArrayList;
    }

    public GameRoom(int gameId, String status, String initializer) {
        this.gameId = gameId;
        this.status = status;
        this.initializer = initializer;
    }



    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getNumPlayers() {
        return numPlayers;
    }

    public void setNumPlayers(int numPlayers) {
        this.numPlayers = numPlayers;
    }

    public int getTimeBeforeGameStarts() {
        return timeBeforeGameStarts;
    }

    public void setTimeBeforeGameStarts(int timeBeforeGameStarts) {
        this.timeBeforeGameStarts = timeBeforeGameStarts;
    }

    public int getTimeBeforeNextRound() {
        return timeBeforeNextRound;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public void setTimeBeforeNextRound(int timeBeforeNextRound) {
        this.timeBeforeNextRound = timeBeforeNextRound;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInitializer() {
        return initializer;
    }

    public void setInitializer(String initializer) {
        this.initializer = initializer;
    }

    public String getWinnner() {
        return winnner;
    }

    public void setWinnner(String winnner) {
        this.winnner = winnner;
    }

    public String getUserLongestWord() {
        return userLongestWord;
    }

    public void setUserLongestWord(String userLongestWord) {
        this.userLongestWord = userLongestWord;
    }

    public String getLongestWord() {
        return longestWord;
    }

    public void setLongestWord(String longestWord) {
        this.longestWord = longestWord;
    }


    public void setMatchPlayersArrayList(ArrayList<MatchPlayers> matchPlayersArrayList) {
        this.matchPlayersArrayList = matchPlayersArrayList;
    }


    public void setMatchInputsArrayList(ArrayList<MatchInputs> matchInputsArrayList) {
        this.matchInputsArrayList = matchInputsArrayList;
    }


    public void setMatchLettersArrayList(ArrayList<MatchLetters> matchLettersArrayList) {
        this.matchLettersArrayList = matchLettersArrayList;
    }

    public void addLettersToMatchLettersArrayList(String letters) {
        MatchLetters matchLetters = new MatchLetters(this.matchLettersArrayList.size() + 1,this.matchLettersArrayList.size() + 1,letters);
     //   System.out.println("Round: " + matchLetters.getRound());
        this.matchLettersArrayList.add(matchLetters);
    }


    public void reduceNumOfPlayers() {
        this.numPlayers = this.numPlayers - 1;
    }
}
