package server_java;

import WordyApp.*;

import client_java.GameGUI;
import org.omg.CORBA.ORB;
import server_java.CommonSql.MatchInputs;
import server_java.CommonSql.MatchLetters;
import server_java.CommonSql.MatchPlayers;
import server_java.game.GameRoom;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

import static java.lang.Thread.sleep;
import static server_java.JavaDB.*;

public class ServerImpl extends WordyPOA {
    private ORB orb;
    public static Connection con;

    ArrayList<GameRoom> gameRoomArrayList = new ArrayList<>();


    public void setORB(ORB orb_val) {
        orb = orb_val;
    }

    @Override
    public TopFivePlayers[] getTopPlayers() {
        return JavaDB.getTopPlayers();
    }

    @Override
    public TopFiveWords[] getTopWords() {
        return JavaDB.getTopFiveWords();
    }

    @Override
    public LogInStatus logIn(String userName, String passWord)  {
        LogInStatus logInStatus = new LogInStatus();
        try {
             logInStatus = JavaDB.logIn(userName,passWord);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        return logInStatus;
    }

    @Override
    public void logOut(String userName) {
        JavaDB.updateStatus(false, userName);
        System.out.println("User: " + userName + " Is now logging out");
    }





    @Override
    public int createOrJoinGame(int userId,String userName) throws SQLException, ClassNotFoundException {
        int gameId = 0;

        //TODO CHECK IF THERE ARE OPEN GAMES
        //Scan the list of games if there is a open one
        for (GameRoom g: gameRoomArrayList){
            if (g.getStatus().equals("Waiting")){
                gameId = g.getGameId();
                //add your self to that ghame
                MatchPlayers m = new MatchPlayers(userId,userName);
                g.addMatchPlayer(m);
                //increment the number of players
                g.addNumPlayer();
                break;
            }
        }

        //if no games were found to be open
        //TODO CREATE NEW GAME
        if (gameId == 0){
            System.out.println("User: " + userName + " Has created a new game");
            gameId = JavaDB.getGamesNumberOfRows();
            GameRoom gameRoom = new GameRoom(gameId,"Waiting",userName);

            //add yourself to the game
            MatchPlayers m = new MatchPlayers(userId,userName);
            gameRoom.addMatchPlayer(m);
            //increment the number of players
            gameRoom.addNumPlayer();
            //set who made the game
            gameRoom.setInitializer(userName);

            gameRoomArrayList.add(gameRoom);
            //TODO add the game to the table
            //note the reason we need this sis so that when noather makes a new game they won't get the same game id
            //this will save us lots of time down the road
            JavaDB.addNewGameToGamesTable(gameRoom);

            //START A THREAD TO CHECK IF THERE WILL BE ENOUGH PLAYERS IN THE NEW GAME
            //THREAD THAT WILL GENERATE THE LETTERS FOR THE NEW GAME
            System.out.println("Thread for GameId: " + gameId + " is starting");
            MyRunnable myRunnable = new MyRunnable(gameId);
            // Create a new thread and pass the instance of MyRunnable to its constructor
            Thread thread = new Thread(myRunnable);
            // Start the thread
            thread.start();
            System.out.println("Thread for GameId: " + gameId + " Has started");
        }

        JavaDB.addToJoinedGames(userId,gameId);
       // System.out.println("The amount of games in the list are: " + gameRoomArrayList.size());
        return gameId;
    }

    //this thread wil handle the game chekcs if its over if its running if someonewon etc
        public class MyRunnable implements Runnable {
            int gameId;
            public MyRunnable(int gameId) {
                this.gameId = gameId;
            }

            //TODO check the number of players in a game
            @Override
            public void run() {
                try {
                    System.out.println("GameId: " + gameId + " Thread is running");
                    boolean willGameRun = false;


                    int size = 0;
                    for (GameRoom g : gameRoomArrayList) {
                        size++;
                        System.out.println("GameId: " + g.getGameId() + "Size: " + size );
                    }

                    for (GameRoom g : gameRoomArrayList) {
                        System.out.println("Current GameId: " + g.getGameId() + " GameId we are looking for: " + gameId);

                        if (g.getGameId() == gameId) {
                            //THIS SECTION JUST CHECKS IF THE GAME WILL RUN
                            System.out.println("GameId: " + g.getGameId() + " Has been found");

                            //TODO start a thread that will generate the letters in advance because we can't wait that long

                            //THREAD THAT WILL GENERATE THE LETTERS FOR THE NEW GAME
                            MyRunnable2 myRunnable2 = new MyRunnable2(gameId);
                            // Create a new thread and pass the instance of MyRunnable to its constructor
                            Thread thread = new Thread(myRunnable2);
                            // Start the thread
                            thread.start();

                            while (true){
                                //Decrement the 10 seconds remaining
                                if (g.getTimeBeforeGameStarts() != 0) {
                                    System.out.println("GameId: " + gameId + " starts in: " + g.getTimeBeforeGameStarts());
                                    g.setTimeBeforeGameStarts(g.getTimeBeforeGameStarts() - 1);
                                } else if (g.getTimeBeforeGameStarts() <= 1) {
                                    //TODO something to check if there are enough players
                                    if (g.getNumPlayers() > 1) {
                                        willGameRun = true;
                                        g.setStatus("In game");
                                        //we get a duplicate of all the players this will be used for later if players leave mid game
                                        g.setRealTimeMatchPlayersArrayList(g.getMatchPlayersArrayList());
                                        JavaDB.updateGameStatusI(g.getGameId());
                                    }
                                    //when the time is up break the loop
                                    break;
                                }

                                try {
                                    sleep(1000);
                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                }

                            }

                            System.out.println("GameId: " + g.getGameId() + " NumOfPlayers: " + g.getNumPlayers() + " Will start: " + willGameRun);

                            //IF THERE ARE ENOUGH PLAYERS THE GAME WILL RUN!!
                            if (willGameRun == true){
                                //TODO SET THE STATUS TO INGAME NO NEED OT UPDATE THE TABLE JUST DO IT AFTER
                                g.setStatus("In game");
                                try {
                                    while (true){
                                        //Decrement the 10 seconds remaining in the match
                                        if (g.getTimeBeforeNextRound() != 0) {
                                 //           System.out.println("GameId: " + gameId + " Current round: " + g.getRound() + " Next round starts in: " + g.getTimeBeforeNextRound());
                                            g.setTimeBeforeNextRound(g.getTimeBeforeNextRound() - 1);
                                        }else if (g.getTimeBeforeNextRound() == 0){
                                            //if there are enough players in the game we will keep checking
                                            if (g.getNumPlayers() > 1){
                                                g.setTimeBeforeNextRound(20);
                                                g.setRound(g.getRound() + 1);
                                                g.updateWinnersPerRound(g.getMatchInputsArrayList());
                                                if (g.getRound() >= 3){
                                                    //if we finally have a winner
                                                    if (!checkForWinner(g.getMatchInputsArrayList()).equals("N/A") ){
                                                        //tell the generate letters thread to stop
                                                        g.setKeepGeneratingLetters(false);
                                                        //TODO check if there is a winner already and reincrement the time per round back to 10 secs
                                                        g.setWinnner(checkForWinner(g.getMatchInputsArrayList()));
                                                        g.setLongestWord(checkForLongestWord(g.getMatchInputsArrayList()));
                                                        g.setUserLongestWord(checkUserForLongestWord(g.getMatchInputsArrayList()));
                                                        g.setStatus("Finished");
                                                        JavaDB.updateFinishedGameToGamesTable(g);
                                                        JavaDB.incrementTotalWinByUsername(checkForWinner(g.getMatchInputsArrayList()));
                                                        break;
                                                    }
                                            /*
                                            else if (g.getNumPlayers() <= 1){
                                                //tell the generate letters thread to stop
                                                g.setKeepGeneratingLetters(false);
                                                //TODO IF THERE IS ONLY ONE PLAYER LEFT
                                                //we get the username of the last player in the game
                                                g.setWinnner(g.getRealTimeMatchPlayersArrayList().get(0).getUserName());
                                                g.setLongestWord(checkForLongestWord(g.getMatchInputsArrayList()));
                                                g.setUserLongestWord(checkUserForLongestWord(g.getMatchInputsArrayList()));
                                                g.setStatus("Finished");
                                                JavaDB.updateFinishedGameToGamesTable(g);
                                                JavaDB.incrementTotalWinByUsername(g.getRealTimeMatchPlayersArrayList().get(0).getUserName());
                                                //TODO if all other players leave mid game do something?
                                                break;
                                            }
                                             */
                                                }
                                            }else {
                                                //tell the generate letters thread to stop
                                                g.setKeepGeneratingLetters(false);
                                                //TODO IF THERE IS ONLY ONE PLAYER LEFT
                                                //we get the username of the last player in the game
                                                g.setWinnner(g.getRealTimeMatchPlayersArrayList().get(0).getUserName());
                                                g.setLongestWord(checkForLongestWord(g.getMatchInputsArrayList()));
                                                g.setUserLongestWord(checkUserForLongestWord(g.getMatchInputsArrayList()));
                                                g.setStatus("Finished");
                                                JavaDB.updateFinishedGameToGamesTable(g);
                                                JavaDB.incrementTotalWinByUsername(g.getRealTimeMatchPlayersArrayList().get(0).getUserName());
                                                //TODO if all other players leave mid game do something?
                                                break;
                                            }
                                        }
                                        sleep(1000);
                                    }

                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                } catch (SQLException e) {
                                    throw new RuntimeException(e);
                                } catch (ClassNotFoundException e) {
                                    throw new RuntimeException(e);
                                }
                                //IF THERE AREN'T ENOUGH PLAYERS THE GAME WIL NOT RUN!!!
                            }else if (willGameRun == false){
                                g.setKeepGeneratingLetters(false);
                                g.setStatus("Dissolved");
                                //TODO UPDATE THE TABLE TO DISSOLVED
                                try {
                                    JavaDB.updateDissolvedGameToGamesTable(g);
                                } catch (SQLException e) {
                                    throw new RuntimeException(e);
                                } catch (ClassNotFoundException e) {
                                    throw new RuntimeException(e);
                                }

                            }


                        }

                        /*
                          try {
                            System.out.println("GameId: " + g.getGameId() + " is now over! ");
                            sleep(2000);
                            //DESTROY THE GAME OBJECT WHEN THE GAME IS OVER!!
                            gameRoomArrayList.remove(g);

                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                         */



                    }
                }catch (ConcurrentModificationException e) {
                    // Handle the exception silently without printing anything
                    System.out.println("GameId: " + gameId + "Concurrent error has occurred");
                }

            }
        }

    public class MyRunnable2 implements Runnable {

        int gameId;
        public MyRunnable2(int gameId) {
            this.gameId = gameId;
        }
        @Override
        public void run() {
            try {
            for (GameRoom g : gameRoomArrayList) {
                if (g.getGameId() == gameId) {
                    while(g.isKeepGeneratingLetters() == true){
                        //we generate 2 letter in advance because the geneerate letters is really slow idk why jarod made it slow
                            String letters = JavaDB.generateLetters();
                            g.addLettersToMatchLettersArrayList(letters);
                       //     System.out.println("GameId: " + g.getGameId() + " Letters generated: " + letters);
                            if (g.isKeepGeneratingLetters() == false){
                                break;
                            }
                            sleep(1000);
                    }
                }
            }

            }catch (SQLException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }catch (ConcurrentModificationException e) {
                // Handle the exception silently without printing anything
             //   System.out.println("GameId: " + gameId + "Has stopped generating letters!");
            }

            System.out.println("Thread generating letters is ending");

        }
    }


    //methdo gets the user that has sent the longest input

    public static String checkUserForLongestWord(ArrayList<MatchInputs> matchInputsArrayList) {
        String userWIthLongestWord = null;
        int longestWordLength = 0;

        //we just use the length no need to get the acutal wrod it self
        for (MatchInputs input : matchInputsArrayList) {
            int inputLength = input.getInput().length();
            if (inputLength > longestWordLength) {
                longestWordLength = inputLength;
                userWIthLongestWord = input.getUserName();
            }
        }

        return userWIthLongestWord;
    }

    //method chekcs for the longest word sent

    private String checkForLongestWord(ArrayList<MatchInputs> matchInputsArrayList) {
        String longestWord = "";

        for (MatchInputs input : matchInputsArrayList) {
            //temp inpuit
            String i = input.getInput();
            if (i.length() > longestWord.length()) {
                longestWord = i;
            }
        }

        return longestWord;
    }

    public static String checkForWinner(ArrayList<MatchInputs> matchInputsArrayList) {
        ArrayList<MatchInputs> longestInputsByRound = new ArrayList<>();
        String winner = "N/A";

        for (MatchInputs input : matchInputsArrayList) {
            boolean hasIdenticalInput = false;

            // Check if the current input has an identical input among the others in the same round
            for (MatchInputs otherInput : matchInputsArrayList) {
                //if same round
                if (input.getRound() == otherInput.getRound() &&
                        //if not the same username
                        !input.getUserName().equals(otherInput.getUserName()) &&
                        //if is a identical
                        input.getInput().equals(otherInput.getInput())) {
                    hasIdenticalInput = true;
                    break;
                }
            }

            // If the input does not have an identical input check if it is the longest input for the current round
            if (!hasIdenticalInput) {
                int round = input.getRound();
                boolean isLongest = true;

                // Check if there is a longer input already stored for the current round
                for (MatchInputs longestInput : longestInputsByRound) {
                    if (longestInput.getRound() == round &&
                            longestInput.getInput().length() >= input.getInput().length()) {
                        isLongest = false;
                        break;
                    }
                }

                // If the input is the longest remove any existing inputs for the same round and add the current input
                if (isLongest) {
                    //only have the longest input per round NOTE m is tthe represnetatioin of MatchInputs
                    //ensures we keep only the lognest word
                    longestInputsByRound.removeIf(m -> m.getRound() == round);
                    longestInputsByRound.add(input);
                }

            }
        }

        // Print the desired output which includes only the longest input in each round that has no identical inputs among the others in the same round
        for (MatchInputs m : longestInputsByRound) {
            System.out.println(m.getId() + " " + m.getRound() + " " + m.getUserName() + " " + m.getInput());
        }

        winner = winnerOfMatch(longestInputsByRound);
        System.out.println("The winner is: " + winner);
        return winner;

    }

    public static String winnerOfMatch(ArrayList<MatchInputs> longestInputsByRound) {
        String winner = "N/A";
        // Count the occurrences of each player in longestInputsByRound
        ArrayList<String> playerNamesWithLongestWords = new ArrayList<>();
        for (MatchInputs input : longestInputsByRound) {
            String name = input.getUserName();
            //if the name doesn't exist in the list don't use it
            if (!playerNamesWithLongestWords.contains(name)) {
                playerNamesWithLongestWords.add(name);
            }
        }

        // Check if any player has spelled the longest word at least three times
        for (String player : playerNamesWithLongestWords) {
            int count = 0;
            for (MatchInputs input : longestInputsByRound) {
                if (input.getUserName().equals(player)) {
                    count++;
                }
            }
            System.out.println("Username: " + player + " Count: " + count);
            if (count >= 3) {
                winner = player;
                break;
            }
        }

        return winner;
    }


    @Override
    public boolean sendGuess(String guess, int gameId, int round, String userName) throws WordGuessException, IOException, SQLException, ClassNotFoundException {

        // Find the game you want to send your guess to using a loop
        for (GameRoom g : gameRoomArrayList) {
            if (g.getGameId() == gameId) {
                if (isWordOnList(guess)){
                    // Get the letters appropriate for that round from the game
                    String letters = g.getLetters(round);
                    // Get all the inputs from the users
                    ArrayList<MatchInputs> matchInputsArrayList = g.getMatchInputsArrayList();

                    //TODO check if this is the correct way to do things
                    // Check if the guess is valid based on the given letters

                    // Check if there is an existing input with the same length from the same user in the same round
                    boolean hasDuplicate = false;
                    for (MatchInputs input : matchInputsArrayList) {
                        if (input.getRound() == round && input.getUserName().equals(userName) &&
                                input.getInput().length() == guess.length()) {
                            hasDuplicate = true;
                            // System.out.println("Guess duplicate:  " + guess + input.getInput());
                            break;
                        }
                    }

                    if (!hasDuplicate) {
                        // If there is no duplicate, add the guess as a new input
                        MatchInputs newInput = new MatchInputs(g.getMatchInputsArrayList().size()+1, round, userName, guess);
                        g.addMatchInput(newInput);
                        //   return true; // Guess is accepted
                    } else {
                        // If there is a duplicate, ignore the guess
                        //   return false; // Guess is ignored
                    }

                }


            }
        }

        if (isWordOnList(guess)) {
            //TODO query here to add the guess to the list
            addWordToGuessTable(gameId,guess,userName);
            return true;
        }else {
            return false;
        }


    }



    // we check the status of a game
    // is it still waiting
    // is it ongoing
    @Override
    public GameStatus checkMatchStatus(GameStatus gameStatus, int gameId) {
        for (GameRoom g : gameRoomArrayList) {
            if (g.getGameId() == gameId) {
                String status = g.getStatus();
                if (status.equalsIgnoreCase("Waiting")) {
                    gameStatus.hasGameStarted = false;
                } else if (status.equalsIgnoreCase("In game")) {
                    gameStatus.hasGameStarted = true;
                } else if (status.equalsIgnoreCase("Dissolved")) {
                    gameStatus.hasGameDissolved = true;
                } else if (status.equalsIgnoreCase("Finished")) {
                    gameStatus.hasGameEnded = true;
                    gameStatus.nameOfWinner = g.getWinnner();
                    if (g.getNumPlayers() <= 1){
                        gameStatus.lastPlayer = true;
                    }
                }
            }
        }

        return gameStatus;
    }

    @Override
    public String getLettersInRound(int gameId, int round){
        String letters = "";
        for (GameRoom g : gameRoomArrayList) {
            if (g.getGameId() == gameId) {
                letters = g.getLetters(round);
                System.out.println("Letters being retrieved: " + letters);
            }
        }
        return letters;
    }

    @Override
    public int getTotalAmountOfWins(String userName) {
        System.out.println("User: " + userName + " is checking the their total wins");
        return JavaDB.showTotalWins(userName);
    }

    @Override
    public void leftGame(int gameId,int userId, String userName ) {
        MatchPlayers m = new MatchPlayers(userId,userName);
        for (GameRoom g : gameRoomArrayList) {
            if (g.getGameId() == gameId) {
                //NOTE WE USE THE OTHER LIST OF PLAYERS SO THAT THE SERVE WILL BE ABLE TO PROCESS THE LAST WINNER CORRECTLY
                g.removeRealTimeMatchPlayer(m);
                g.reduceNumOfPlayers();
            }
        }
        System.out.println(userName + " Has left gameId: " + gameId);
    }

    @Override
    public int getTimeBeforeGameStarts(int gameId) {
        for (GameRoom g : gameRoomArrayList) {
            if (g.getGameId() == gameId) {
                return g.getTimeBeforeGameStarts();

            }
        }
        return 0;
    }

    @Override
    public RoundDetails getRoundDetails(int gameId) {
        RoundDetails rd = new RoundDetails();
        for (GameRoom g : gameRoomArrayList) {
            if (g.getGameId() == gameId) {
                rd.round = g.getRound();
                rd.timeLeftInRound = g.getTimeBeforeNextRound();
                rd.letters = g.getLetters(rd.round);
            }
        }
        return rd;
    }

    @Override
    public String getWinnerOfRound(int round, int gameId) {
        String winner ="N/A";
        for (GameRoom g: gameRoomArrayList){
            if (g.getGameId() == gameId) {
            //    System.out.println("A match was ounf looking for the winner of the round " + gameId);
                winner = g.checkWinnerOfRound(round);
            }
        }
        return winner;
    }


}




