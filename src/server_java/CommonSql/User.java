package server_java.CommonSql;

public class User {
    private int userId;
    private String userName;

    private String password;
    private String status;
    private String totalWin;

    public User(int userId, String userName, String password, String status, String totalWin) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
        this.status = status;
        this.totalWin = totalWin;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotalWin() {
        return totalWin;
    }

    public void setTotalWin(String totalWin) {
        this.totalWin = totalWin;
    }



}
