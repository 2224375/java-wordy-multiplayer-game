package server_java.CommonSql;

public class MatchLetters {
    public MatchLetters(int id, int round, String letters) {
        this.id = id;
        this.round = round;
        this.letters = letters;
    }

    private int id;
    private int round;
    private String letters;



    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }
}
