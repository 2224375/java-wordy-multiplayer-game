package server_java.CommonSql;

public class MatchInputs {


    private int id;
    private int round;
    private String userName;
    private String input;
    public MatchInputs(int id, int round, String userName, String input) {
        this.id = id;
        this.round = round;
        this.userName = userName;
        this.input = input;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }



}
