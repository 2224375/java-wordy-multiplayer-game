package server_java.CommonSql;

public class Match {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;
    private String userName;
    private int round;
    private String winner;
    private String longestWord;

    public Match(int id, String userName, String winner, String longestWord, int round) {
        this.id = id;
        this.userName = userName;
        this.round = round;
        this.winner = winner;
        this.longestWord = longestWord;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }


    public String getLongestWord() {
        return longestWord;
    }

    public void setLongestWord(String longestWord) {
        this.longestWord = longestWord;
    }
}
