package server_java.CommonSql;

public class MatchPlayers {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public MatchPlayers(int id, String userName) {
        this.id = id;
        this.userName = userName;
    }

    private String userName;
}
