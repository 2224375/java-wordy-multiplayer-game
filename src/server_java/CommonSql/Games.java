package server_java.CommonSql;

public class Games {


    public String getUserLongWord() {
        return userLongWord;
    }

    public void setUserLongWord(String userLongWord) {
        this.userLongWord = userLongWord;
    }

    public Games(int gameId, String initializer, String status, int numPlayers, String winners, String userLongWord, String longestWord) {
        this.gameId = gameId;
        this.initializer = initializer;
        this.status = status;
        this.numPlayers = numPlayers;
        this.winners = winners;
        this.userLongWord = userLongWord;
        this.longestWord = longestWord;
    }





    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public String getInitializer() {
        return initializer;
    }

    public void setInitializer(String initializer) {
        this.initializer = initializer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNumPlayers() {
        return numPlayers;
    }

    public void setNumPlayers(int numPlayers) {
        this.numPlayers = numPlayers;
    }

    public String getWinners() {
        return winners;
    }

    public void setWinners(String winners) {
        this.winners = winners;
    }

    public String getLongestWord() {
        return longestWord;
    }

    public void setLongestWord(String longestWord) {
        this.longestWord = longestWord;
    }

    private int gameId;
    private String initializer;
    private String status;
    private int numPlayers;
    private String winners;

    private String userLongWord;


    private String longestWord;
}
