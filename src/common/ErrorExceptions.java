package common;
import org.omg.CORBA.*;

public class ErrorExceptions {

    /*
    class ObjectNotExist {
        public static void main(String args[]) {

            try {
                // Create ORB object
                ORB orb = ORB.init(args, null);

                // Get object reference
                org.omg.CORBA.Object objRef = orb.string_to_object("corbaname::localhost:1050/MyCORBAObject");

                // Narrow object reference
                MyCORBAObject myObj = MyCORBAObjectHelper.narrow(objRef);

                // Call method on remote object
                myObj.someMethod();

            } catch (org.omg.CORBA.OBJECT_NOT_EXIST ex) {
                System.err.println("Object does not exist: " + ex.getMessage());
                ex.printStackTrace();
            } catch (Exception ex) {
                System.err.println("Exception occurred: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }


    class Transient {
        public static void main(String args[]) {

            try {
                // Create ORB object
                ORB orb = ORB.init(args, null);

                // Get object reference
                org.omg.CORBA.Object objRef = orb.string_to_object("corbaname::localhost:1050/MyCORBAObject");

                // Narrow object reference
                MyCORBAObject myObj = MyCORBAObjectHelper.narrow(objRef);

                // Call method on remote object
                myObj.someMethod();

            } catch (org.omg.CORBA.TRANSIENT ex) {
                System.err.println("Connection lost or server is unavailable: " + ex.getMessage());
                ex.printStackTrace();
            } catch (Exception ex) {
                System.err.println("Exception occurred: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }


    class CommFail {
        public static void main(String args[]) {

            try {
                // Create ORB object
                ORB orb = ORB.init(args, null);

                // Get object reference
                org.omg.CORBA.Object objRef = orb.string_to_object("corbaname::localhost:1050/MyCORBAObject");

                // Narrow object reference
                MyCORBAObject myObj = MyCORBAObjectHelper.narrow(objRef);

                // Call method on remote object
                myObj.someMethod();

            } catch (org.omg.CORBA.COMM_FAILURE ex) {
                System.err.println("Communication failure: " + ex.getMessage());
                ex.printStackTrace();
            } catch (Exception ex) {
                System.err.println("Exception occurred: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }


    class Marshal {
        public static void main(String args[]) {

            try {
                // Create ORB object
                ORB orb = ORB.init(args, null);

                // Get object reference
                org.omg.CORBA.Object objRef = orb.string_to_object("corbaname::localhost:1050/MyCORBAObject");

                // Narrow object reference
                MyCORBAObject myObj = MyCORBAObjectHelper.narrow(objRef);

                // Call method on remote object
                myObj.someMethod();

            } catch (org.omg.CORBA.MARSHAL ex) {
                System.err.println("Error marshaling arguments or results: " + ex.getMessage());
                ex.printStackTrace();
            } catch (Exception ex) {
                System.err.println("Exception occurred: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }


    public class Internal {
        public static void main(String args[]) {

            try {
                // Create ORB object
                ORB orb = ORB.init(args, null);

                // Get object reference
                org.omg.CORBA.Object objRef = orb.string_to_object("corbaname::localhost:1050/MyCORBAObject");

                // Narrow object reference
                MyCORBAObject myObj = MyCORBAObjectHelper.narrow(objRef);

                // Call method on remote object
                myObj.someMethod();

            } catch (org.omg.CORBA.INTERNAL ex) {
                System.err.println("Internal CORBA error: " + ex.getMessage());
                ex.printStackTrace();
            } catch (Exception ex) {
                System.err.println("Exception occurred: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }


    public class NoPermission {
        public static void main(String args[]) {

            try {
                // Create ORB object
                ORB orb = ORB.init(args, null);

                // Get object reference
                org.omg.CORBA.Object objRef = orb.string_to_object("corbaname::localhost:1050/MyCORBAObject");

                // Narrow object reference
                MyCORBAObject myObj = MyCORBAObjectHelper.narrow(objRef);

                // Call method on remote object
                myObj.someMethod();

            } catch (org.omg.CORBA.NO_PERMISSION ex) {
                System.err.println("No permission to perform the operation: " + ex.getMessage());
                ex.printStackTrace();
            } catch (Exception ex) {
                System.err.println("Exception occurred: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }


    public class BadOperation {
        public static void main(String args[]) {

            try {
                // Create ORB object
                ORB orb = ORB.init(args, null);

                // Get object reference
                org.omg.CORBA.Object objRef = orb.string_to_object("corbaname::localhost:1050/MyCORBAObject");

                // Narrow object reference
                MyCORBAObject myObj = MyCORBAObjectHelper.narrow(objRef);

                // Call method on remote object
                myObj.someMethod();

            } catch (org.omg.CORBA.BAD_OPERATION ex) {
                System.err.println("Bad operation: " + ex.getMessage());
                ex.printStackTrace();
            } catch (Exception ex) {
                System.err.println("Exception occurred: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }


     */
}

