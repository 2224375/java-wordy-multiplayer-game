package common;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class contains read and write methods.
 */

public class ReadAndWrite {
    /**
     * Reads words from a text file and stores them in an arraylist.
     * @return List of words
     * @throws FileNotFoundException If the file was not found
     */
    public ArrayList readWordsFromText() throws FileNotFoundException {
        ArrayList<String> wordList = new ArrayList<>();
        File file = new File("resources/games/words.txt");
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            wordList.add(scanner.nextLine());
        }
        return wordList;
    }

    //throws IOException was only added so that the program will compile correctly

    /**
     * Writes the accounts to a text file.
     * @return Added only so that the program will compile correctly
     * @throws IOException If there was an error writing to the text file.
     */
    public Class writeWordsToText() throws IOException {
           FileWriter writer = new FileWriter("resources/games/User.txt");
           Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your username: ");
        String username = scanner.nextLine();
        System.out.println("Checking to see if username exists...");
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader("resources/games/User.txt"));
            String line;
            boolean usernameExists = false;
            while((line = bufferedReader.readLine()) != null) {
                if (line.equals(username)) {
                    usernameExists = true;
                    break;
                }
            }
            if (usernameExists) {
                System.out.println("Username exists! Please try again.");
            } else {
                System.out.println("Username accepted");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //return null was only added so that the program will compile correctly
        return null;
    }
}
