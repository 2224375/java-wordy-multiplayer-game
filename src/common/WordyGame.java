package common;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class WordyGame {
    public static int vowelCount = 7;
    public static final int consonantCount = 10;
    public static final int minWordLength = 5;
    public static final String vowels = "aeiou";
    public static final String consonants = "bcdfghjklmnpqrstvwxyz";
    public static final Random rand = new Random();

    public static void main(String[] args) throws Exception {
        playGame();
    }

    public static void playGame() throws Exception {
        ArrayList<String> words = loadWordsFromFile();
        //TODO: SQLConnection to Database MAKE SURE THE LETTERS GIVEN WILL BE ABLE TO FORM A WORD !!!
        StringBuilder vowelLetters = generateRandomLetters(vowels, vowelCount, rand);
        StringBuilder consonantLetters = generateRandomLetters(consonants, consonantCount, rand);

        System.out.println("Vowels: " + vowelLetters.toString());
        System.out.println("Consonants: " + consonantLetters.toString());

        StringBuilder letters = combineLetters(vowelLetters, consonantLetters);

        while (!canFormWord(words, letters)) {
            vowelLetters = generateRandomLetters(vowels, vowelCount, rand);
            consonantLetters = generateRandomLetters(consonants, consonantCount, rand);
            letters = combineLetters(vowelLetters, consonantLetters);
            System.out.println("Vowels: " + vowelLetters.toString());
            System.out.println("Consonants: " + consonantLetters.toString());
        }

        String tempUniqueVowels = String.valueOf(vowelLetters);
        String tempUniqueConsonants = String.valueOf(consonantLetters);

        //get only the unique vowels and consonants
        String uniqueVowels = tempUniqueVowels.replaceAll("(.)(?=.*\\1)", "");
        String uniqueConsonants = tempUniqueConsonants.replaceAll("(.)(?=.*\\1)", "");

        ArrayList<CharOccurrence> vCharOccrrList = getUniqueOccurrence(tempUniqueVowels, uniqueVowels);
        ArrayList<CharOccurrence> cCharOccrrList = getUniqueOccurrence(tempUniqueConsonants, uniqueConsonants);

        int count = countValidWords(words, letters);
    //    System.out.println("Available Words that can be formed: " + count);

        System.out.println("\n");
        System.out.println("=============");
        System.out.println("vowels");
        for (CharOccurrence v : vCharOccrrList) {
            System.out.print(v.letter);
        }
        System.out.println("");
        for (CharOccurrence v : vCharOccrrList) {
            System.out.print(v.occurrence);
        }
        System.out.println("");
        System.out.println("consonants:");
        for (CharOccurrence c : cCharOccrrList) {
            System.out.print(c.letter);
        }
        System.out.println("");
        for (CharOccurrence c : cCharOccrrList) {
            System.out.print(c.occurrence);
        }
        System.out.println("");
        System.out.println("=============");

        while (true) {
            boolean wordIsValid = false;
            String word = promptUserForWord(letters);
            while (!wordIsValid) {
                if (isValidWord(word, letters) && words.contains(word)) {
                    wordIsValid = true;
                    System.out.println("Valid word!");
                } else {
                    System.out.println("Invalid word. Please enter a valid word.");
                    word = promptUserForWord(letters);
                }
            }
        }
    }

    //method for file loader
    public static ArrayList<String> loadWordsFromFile() throws IOException {
        ArrayList<String> words = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader("resources/games/words.txt"));
        String line;
        while ((line = reader.readLine()) != null) {
            words.add(line.trim().toLowerCase());
        }
        reader.close();
        return words;
    }

    //method for letter generator
    public static StringBuilder generateRandomLetters(String letters, int count, Random rand) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < count; i++) {
            result.append(letters.charAt(rand.nextInt(letters.length())));
        }
        return result;
    }

    //method to combines vowel and consonant objects into a single StringBuilder object
    public static StringBuilder combineLetters(StringBuilder vowelLetters, StringBuilder consonantLetters) {
        StringBuilder letters = new StringBuilder();
        letters.append(vowelLetters);
        letters.append(consonantLetters);
        return letters;
    }

    // Modified method for accepting valid word input from the user
    public static String promptUserForWord(StringBuilder letters) {
        Scanner scanner = new Scanner(System.in);
        String word;
        do {
            System.out.println("Enter a " + minWordLength + "-letter word using the given letters: ");
            word = scanner.nextLine().toLowerCase().trim();
            if (minWordLength > word.length()) {
                System.out.println("Invalid word length. Please enter at least " + minWordLength + "-letter word.");
                word = null;
            } else {
                boolean valid = true;
                Map<Character, Integer> letterCounts = new HashMap<>();
                for (char c : letters.toString().toCharArray()) {
                    letterCounts.put(c, letterCounts.getOrDefault(c, 0) + 1);
                }
                for (char c : word.toCharArray()) {
                    if (!letterCounts.containsKey(c) || letterCounts.get(c) <= 0) {
                        valid = false;
                        System.out.println("Invalid word. Please use only the given letters.");
                        break;
                    }
                    letterCounts.put(c, letterCounts.get(c) - 1);
                }
                if (!valid) {
                    word = null;
                }
            }
        } while (word == null);

        return word;
    }

    // Modified method to check if word is valid
    public static boolean isValidWord(String word, StringBuilder letters) {
        if (word == null || word.isEmpty()) {
            return false;
        }

        Map<Character, Integer> letterCounts = new HashMap<>();
        for (char c : letters.toString().toCharArray()) {
            letterCounts.put(c, letterCounts.getOrDefault(c, 0) + 1);
        }

        for (char c : word.toCharArray()) {
            if (!letterCounts.containsKey(c) || letterCounts.get(c) <= 0) {
                return false;
            }
            letterCounts.put(c, letterCounts.get(c) - 1);
        }

        return true;
    }


    //method counts the amount of times a character has appeared
    public static ArrayList<CharOccurrence> getUniqueOccurrence(String tempUniqueVowels, String uniqueVowels) {
        ArrayList<CharOccurrence> charOccrList = new ArrayList<>();
        // These two for loops will extract the occurrence of each character from the
        for (int i = 0; i < uniqueVowels.length(); i++) {
            Character temp = uniqueVowels.charAt(i);
            CharOccurrence oc = new CharOccurrence(temp, 0);
            int occr = 0;
            for (int x = 0; x < tempUniqueVowels.length(); x++) {
                if (temp.equals(tempUniqueVowels.charAt(x))) {
                    occr++;
                }
            }
            oc.setOccurrence(occr);
            charOccrList.add(oc);
        }
        return charOccrList;
    }

    //method for counting available words
    public static boolean canFormWord(ArrayList<String> words, StringBuilder letters) {
        return countValidWords(words, letters) >= 1000;
    }

    //method for counting available words
    public static int countValidWords(ArrayList<String> words, StringBuilder letters) {
        int count = 0;
        for (String word : words) {
            if (isValidWord(word, letters)) {
                count++;
            }
        }
        return count;
    }
}