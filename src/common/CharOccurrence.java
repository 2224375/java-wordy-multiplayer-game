package common;

public class CharOccurrence {
    public CharOccurrence(Character letter, int occurrence) {
        this.letter = letter;
        this.occurrence = occurrence;
    }

    Character letter;

    public Character getLetter() {
        return letter;
    }

    public void setLetter(Character letter) {
        this.letter = letter;
    }

    public int getOccurrence() {
        return occurrence;
    }

    public void setOccurrence(int occurrence) {
        this.occurrence = occurrence;
    }

    int occurrence;
}
