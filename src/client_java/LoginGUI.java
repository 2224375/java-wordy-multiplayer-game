package client_java;

import WordyApp.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class LoginGUI extends JDialog implements ActionListener {
    private JFrame frame = new JFrame();
    private JPanel contentPane;
    private JButton loginButton;
    private JButton exitButton;
    private JPasswordField passwordField;
    private JTextField usernameField;
    private JLabel messageLabel;
    static Wordy serverImpl;


    public LoginGUI(Wordy serv) {
        serverImpl = serv;
        frame.add(contentPane);
        setContentPane(contentPane);
        setModal(true);
        setSize(900, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        loginButton.addActionListener(this);
        exitButton.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        LogInStatus logInStatus = null;
        frame.dispose();
        if (e.getSource() == loginButton) {
            String username = usernameField.getText();
            String password = new String(passwordField.getPassword());

            try {
                logInStatus = serverImpl.logIn(username, password);

                if (logInStatus.doesUserExist) {
                    System.out.println("User exists in the database");
                    if (logInStatus.isPassWordCorrect) {
                        System.out.println("Username and password are correct");
                        if (!logInStatus.isUserOnline) {
                            System.out.println("User is not currently online");
                            dispose();
                            new AnotherMenuGUI(serverImpl, username, logInStatus.id);
                        } else {
                            throw new UserIsOnlineException("Cannot login, please try again later");
                        }
                    } else {
                        throw new WrongPasswordException("Username or password does not match");
                    }
                } else {
                    throw new UserDoesNotExist("Username or password does not match");
                }
            } catch (UserIsOnlineException ex) {
                JOptionPane.showMessageDialog(null, "Cannot login, please try again later", "Wordy", JOptionPane.ERROR_MESSAGE);
            } catch (WrongPasswordException ex) {
                JOptionPane.showMessageDialog(null, "Username or password does not match", "Wordy", JOptionPane.ERROR_MESSAGE);
            } catch (UserDoesNotExist ex) {
                JOptionPane.showMessageDialog(null, "Username or password does not match", "Wordy", JOptionPane.ERROR_MESSAGE);
            }
        } else if (e.getSource() == exitButton) {
            System.exit(0);
        }
    }




}
