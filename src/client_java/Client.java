package client_java;

// import common.WordyApp.*;
import WordyApp.Wordy;
import WordyApp.WordyHelper;
import org.omg.CosNaming.*;
import org.omg.CORBA.*;
public class Client {
    //this is the stub
    static Wordy serverImpl;

    public static void main(String[] args) {
        try {
            ORB orb = ORB.init(args, null);
            org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
            String name = "Wordy";
            serverImpl = WordyHelper.narrow(ncRef.resolve_str(name));
            System.out.println("[JAVA CLIENT] Starting the client...");
            // prompt the user with LoginGUI
            LoginGUI dialog = new LoginGUI(serverImpl);
            dialog.pack();
            dialog.setTitle("Wordy-LoginGUI");
            dialog.setSize(900,600);
            dialog.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}
