package client_java;


import WordyApp.Wordy;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;

/**
 * This class is used to create a GUI for the main menu
 */
public class AnotherMenuGUI {
    private JFrame frame = new JFrame();
    private JLabel welcomeUserLabel, userTotalWins, titleLabel, backgroundImageLabel;
    private ImageIcon icon;
    private JButton startGameButton, logoutButton, leaderboardsButton;

    static Wordy serverImpl;
    String userName;
    int userId;



    /**
     * Constructor for initializing instance variables
     * @param serv
     * @param name
     */
    public AnotherMenuGUI(Wordy serv, String name,int id) {
        serverImpl = serv;
        userName = name;
        userId = id;
        System.out.println(userId);


        //add da shutdown hook to call serverImpl.logOut(userName) before program termination
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            serverImpl.logOut(userName);
        }));


        int totalWins = serverImpl.getTotalAmountOfWins(userName);

        //frame.setPreferredSize(new Dimension(640, 480));
        frame.setSize(900,600);
        frame.setTitle("Wordy - A GameRoom");
        frame.setLayout(null);
        frame.setLocationRelativeTo(null);

        // Window icon
        icon = new ImageIcon("src/client_java/logo-no-background.png");
        frame.setIconImage(icon.getImage());

        // Background image label
        backgroundImageLabel = new JLabel("", new ImageIcon("src/client_java/background-2.gif"),
                JLabel.CENTER);
        backgroundImageLabel.setBounds(-8,  0, 900, 600);

        // Title label
        titleLabel = new JLabel("WORDY- A GAME");
        titleLabel.setForeground(new Color(55, 55, 55, 255));
        titleLabel.setFont(new Font("COPPERPLATE GOTHIC BOLD", Font.BOLD, 32));
        titleLabel.setBounds(290, 150, 350, 33);
        titleLabel.setHorizontalTextPosition(JLabel.CENTER);

        welcomeUserLabel = new JLabel("Welcome! " + userName);
        welcomeUserLabel.setFont(new Font("Arial", Font.BOLD, 18));
        welcomeUserLabel.setBounds(370,145,300,100);
        welcomeUserLabel.setHorizontalTextPosition(JLabel.CENTER);

        userTotalWins = new JLabel("Your total wins " + totalWins);
        userTotalWins.setFont(new Font("Arial", Font.BOLD, 18));
        userTotalWins.setBounds(550,395,250,100);
        userTotalWins.setHorizontalTextPosition(JLabel.CENTER);

        // Start game button
        startGameButton = new JButton("Create/Join game");
        startGameButton.addActionListener(this::startGameButtonClicked);
        startGameButton.setBounds(355, 240, 165, 30);

        // Leaderboards button
        leaderboardsButton = new JButton("Leaderboards");
        leaderboardsButton.addActionListener(this::leaderboardsButtonClicked);
        leaderboardsButton.setBounds(355, 275, 165, 30);

        // Logout game button
        logoutButton = new JButton("Logout");
        logoutButton.addActionListener(this::logoutButtonClicked);
        logoutButton.setBounds(355, 310, 165, 30);

        // Adding of components
        frame.add(titleLabel);
        frame.add(welcomeUserLabel);
        frame.add(userTotalWins);
        frame.add(startGameButton);
        frame.add(logoutButton);
        frame.add(leaderboardsButton);
        frame.add(backgroundImageLabel);


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    /**
     * Creates a game or joins an existing game
     * @param e Action performed
     */
    public void startGameButtonClicked(ActionEvent e)  {
        try {
            frame.dispose();
            int gameId = 0;
            if (e.getSource() == startGameButton) {

                gameId = serverImpl.createOrJoinGame(userId,userName);

                System.out.println("[CLIENT] The gameId you have joined in is: " + gameId);
                //for testing purposes only!
                GameGUI dialog = new GameGUI(serverImpl, userName, gameId,userId);
                dialog.pack();
                dialog.setTitle("Wordy GameRoom");
                dialog.setLocationRelativeTo(null);
                dialog.setSize(900,600);
                dialog.setVisible(true);


                //TODO: Start/Create game button functionality
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        }

    }

    /**
     * Logs the user out of the game and disposed the frame
     * @param e Action performed
     */
    public void logoutButtonClicked(ActionEvent e) {
        if (e.getSource() == logoutButton) {
            serverImpl.logOut(userName);
            frame.dispose();
        }
    }

    /**
     * Shows the leaderboards GUI
     * @param e Action performed
     */
    public void leaderboardsButtonClicked(ActionEvent e) {
        if (e.getSource() == leaderboardsButton) {
            frame.dispose();
            new AnotherLeaderboardGUI(serverImpl, userName,userId);
        }
    }
}

