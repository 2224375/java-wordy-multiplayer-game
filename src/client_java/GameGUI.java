package client_java;

import WordyApp.*;


import javax.swing.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Thread.sleep;

public class GameGUI extends JDialog {
    private JPanel contentPane;
    private JLabel consonants;
    private JLabel round;
    private JLabel timeLeft;
    private JLabel vowels;
    private JButton exitButton;
    private JTextField guessTextField;
    private JButton guessButton;
    private JLabel errorTextField;
    private JLabel wordGuessDesc;
    private JLabel RoundDesc;
    private JLabel TimeDesc;
    private JLabel LettersDesc;
    private JLabel ConsonantsDesc;
    private JLabel winnerDesc;
    private JLabel Round;
    private JLabel TimeLeft;
    private JLabel Letters;
    private JButton buttonOK;
    private JButton buttonCancel;

    private Wordy serverImpl;
    private String userName;
    private int gameId;
    private int matchRound;

    private String vowelsFromServer;
    private String consonantsFromServer;
    private static GameStatus gameStatus = new GameStatus();
    //NOTE THIS IS HERE SO THAT WE CAN SET THIS TO TRUE JUST FOR ONCE
    private Boolean gameStart;
    private String letters;

    public static final int minWordLength = 5;
    public static final int maxWordLength = 17;
    private int userId;

    private int timeBeforeGameStarts;

    public GameGUI(Wordy server, String uName, int gId,int id) {
        gameStatus.hasGameEnded = false;
        gameStatus.hasGameStarted = false;
        gameStatus.hasGameDissolved = false;
        gameStatus.lastPlayer = false;

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            serverImpl.logOut(userName);
        }));

        this.serverImpl = server;
        this.userName = uName;
        this.gameId = gId;
        this.userId = id;
        setContentPane(contentPane);
        setModal(true);
        setLocationRelativeTo(null);
        getRootPane().setDefaultButton(buttonOK);


        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        //THIS THREAD CHECKS THE STATUS OF THE GAME!!!!!
        // Create an instance of MyRunnable
        MyRunnable myRunnable = new MyRunnable();
        // Create a new thread and pass the instance of MyRunnable to its constructor
        Thread thread = new Thread(myRunnable);
        // Start the thread
        thread.start();


        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        guessButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //send a guess
                if (e.getSource() == guessButton) {
                    try {
                        System.out.println(guessTextField.getText());
                        System.out.println(gameId);
                        System.out.println(userName);

                        if (gameStatus.hasGameEnded == false){
                            //check if the words conform to the minimum and maximum length of characters
                            if (checkGuessLength(guessTextField.getText())){

                                //check if the words usses the correct amount and type of characters
                                if (checkGuessLetters(new StringBuilder(letters),guessTextField.getText())){

                                    //send the guess to the server to check if the word exist in the list of possible matches
                                    Boolean isGuessRight = serverImpl.sendGuess(guessTextField.getText(), gameId, matchRound, userName);

                                    if (isGuessRight){
                                        wordGuessDesc.setText("The guess was correct!");
                                    }else {
                                        wordGuessDesc.setText("The guess was incorrect!");
                                        throw new WordGuessException("The word guessed does not exist!");
                                    }



                                }else {
                                    wordGuessDesc.setText("Invalid word. Please use only the given letters.");
                                    throw new WordGuessException("The guess you have attempted to send might be using letters that are not allotted for this round");
                                }

                                //1st if
                            }else {
                                wordGuessDesc.setText("Invalid word length. Please enter at least 5 letters or less than 17 letters");
                                throw new GuessLengthException("The guess you would send to the server does not conform with the given criteria");
                            }
                        }else {
                            wordGuessDesc.setText("You cannot guess anymore game is over!");
                            throw new GameOverException("GameRoom has already ended, server will not accept anymore guesses is not allowed!");
                        }


                    } catch (Exception a) {
                        a.printStackTrace();
                    }
                }
            }

        });
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == exitButton) {
                    //KILL ALL THE THREADS THAT ARE STILL RUNNING !!!!!
                    serverImpl.leftGame(gameId, userId,userName);
                    gameStatus.hasGameEnded = true;
                    System.out.println("Going back to the the main menu");
                    dispose();
                    new AnotherMenuGUI(serverImpl, userName,userId);
                }


            }
        });
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }


    //TODO
    // CHECK IF THE GAME HAS STARTED IF NOT THEN WE OPT OUT ELSE ELSE WE STAR THE THREAD
    // THREAD MUST GET THE LETTERS PER ROUND
    // AND CHECK IF THE ROUND/GAME IS OVER

    public class MyRunnable implements Runnable {
        @Override
        public void run() {
            System.out.println("Thread is running");
            //TODO loop that will check if the game has started or was dissolved
            while (true) {
                //we check if the game has started
                gameStatus = serverImpl.checkMatchStatus(gameStatus, gameId);
                winnerDesc.setText("Time left before game starts: " + serverImpl.getTimeBeforeGameStarts(gameId));

                //doot
                //if the game has started we break the loop
                if (gameStatus.hasGameStarted == true) {
                    //This thread is for the timer
                    winnerDesc.setText("The game has started");
                    System.out.println("[CLIENT] Thread for the timer each round is running");
                    MyRunnable2 myRunnable2 = new MyRunnable2();
                    Thread thread1 = new Thread(myRunnable2);
                    thread1.start();
                    break;
                    //we check if the game has been dissolved
                } else if (gameStatus.hasGameDissolved == true) {
                    //TODO prompt here will tell you something about. The game ending because there was not enough players
                    System.out.println("[CLIENT] Not enough players to start the game Thread for the time will not start!");
                    winnerDesc.setText("GameRoom is empty! Not enough players have joined.");
                    try {
                        throw new NotEnoughPlayersException("GameRoom will not start due to the insufficient amount of players that have joined");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                    //we check if the game has been finished
                }
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

            }

            boolean stopMainMenuDupe = false;

            while (gameStatus.hasGameEnded != true) {
                gameStatus = serverImpl.checkMatchStatus(gameStatus, gameId);
                if (gameStatus.lastPlayer == true){
                    //kill all the other threads
                    gameStatus.hasGameEnded = true;
                    winnerDesc.setText("All other players have left. You have won: " + userName);
                    break;
                }
                if (gameStatus.hasGameEnded == true) {
                    // if it's your name i guess you could write "Congratulations you won!"
                    String nameOfWinner = gameStatus.nameOfWinner;
                    winnerDesc.setText("GameRoom Over! The winner of the game is: " + nameOfWinner );

                    //TODO timer here to countdown to return back to the the main menu
                    int goBackMain = 5;
                    Round.setText("");
                    RoundDesc.setText("");
                    Letters.setText("");

                    LettersDesc.setText("");
                    TimeLeft.setText("");
                    TimeDesc.setText("");

                    guessTextField.setVisible(false);
                    guessButton.setVisible(false);

                    while (goBackMain != 0){
                        wordGuessDesc.setText("Going back to the main menu in: " + String.valueOf(goBackMain));
                        goBackMain--;
                        try {
                            sleep(1000);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }

                    }

                    //TODO go back to the maine menu
                    //KILL ALL THE THREADS THAT ARE STILL RUNNING !!!!!
                    System.out.println("Going back to the the main menu");
                    if (stopMainMenuDupe == false){
                        dispose();
                        new AnotherMenuGUI(serverImpl, userName,userId);
                    }

                    stopMainMenuDupe = true;



                    break;
                }

                try {
                    sleep(1000); // Delay for 1 second (adjust as needed)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("[CLIENT] Thread Checking GameRoom status is ending");
        }
    }

    /*
         boolean stopMainMenuDupe = false;

            while (gameStatus.hasGameEnded != true) {
                gameStatus = serverImpl.checkMatchStatus(gameStatus, gameId);
                if (gameStatus.lastPlayer == true){
                    //kill all the other threads
                    gameStatus.hasGameEnded = true;
                    winnerDesc.setText("All other players have left. You have won: " + userName);
                    break;
                }
                if (gameStatus.hasGameEnded == true) {
                    // if it's your name i guess you could write "Congratulations you won!"
                    String nameOfWinner = gameStatus.nameOfWinner;
                    winnerDesc.setText("GameRoom Over! The winner of the game is: " + nameOfWinner );

                    //TODO timer here to countdown to return back to the the main menu
                    int goBackMain = 5;
                    Round.setText("");
                    RoundDesc.setText("");
                    Letters.setText("");

                    LettersDesc.setText("");
                    TimeLeft.setText("");
                    TimeDesc.setText("");

                    guessTextField.setVisible(false);
                    guessButton.setVisible(false);

                    while (goBackMain != 0){
                        wordGuessDesc.setText("Going back to the main menu in: " + String.valueOf(goBackMain));
                        goBackMain--;
                        try {
                            sleep(1000);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }

                    }

                    //TODO go back to the maine menu
                    //KILL ALL THE THREADS THAT ARE STILL RUNNING !!!!!
                    System.out.println("Going back to the the main menu");
                    if (stopMainMenuDupe == false){
                        dispose();
                        new AnotherMenuGUI(serverImpl, userName,userId);
                    }

                    stopMainMenuDupe = true;



                    break;
                }

                try {
                    sleep(1000); // Delay for 1 second (adjust as needed)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("[CLIENT] Thread Checking GameRoom status is ending");
        }
    }
     */


    /*
    while (gameStatus.hasGameEnded != true) {
                gameStatus = serverImpl.checkMatchStatus(gameStatus, gameId);
                if (gameStatus.lastPlayer == true){
                    //kill all the other threads
                    gameStatus.hasGameEnded = true;
                    winnerDesc.setText("All other players have left. You have won: " + userName);
                    break;
                }
                if (gameStatus.hasGameEnded == true) {

                    // if it's your name i guess you could write "Congratulations you won!"
                    String nameOfWinner = gameStatus.nameOfWinner;
                    winnerDesc.setText("GameRoom Over! The winner of the game is: " + nameOfWinner );

                    //TODO timer here to countdown to return back to the the main menu
                    int goBackMain = 5;
                    Round.setText("");
                    RoundDesc.setText("");
                    Letters.setText("");

                    LettersDesc.setText("");
                    TimeLeft.setText("");
                    TimeDesc.setText("");

                    guessTextField.setVisible(false);
                    guessButton.setVisible(false);

                    while (goBackMain != 0){
                        wordGuessDesc.setText("Going back to the main menu in: " + String.valueOf(goBackMain));
                        goBackMain--;
                        try {
                            sleep(1000);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }

                    }

                    //TODO go back to the maine menu
                    //KILL ALL THE THREADS THAT ARE STILL RUNNING !!!!!
                    System.out.println("Going back to the the main menu");
                    dispose();
                    new AnotherMenuGUI(serverImpl, userName,userId);



                    break;
                }

                try {
                    sleep(1000); // Delay for 1 second (adjust as needed)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("[CLIENT] Thread Checking GameRoom status is ending");
        }
    }
     */











    //TODO THE TIMER FOR EACH ROUND
    public class MyRunnable2 implements Runnable {
        @Override
        public void run() {
            try {
                boolean isOver = false;
                //while the game has not ended then we just keep running the timer
                while (gameStatus.hasGameEnded != true) {
                    //TODO get the letters round and time left
                    while (true) {
                        RoundDetails rd = new RoundDetails();
                        rd = serverImpl.getRoundDetails(gameId);

                        letters = rd.letters;
                        matchRound = rd.round;

                        //set the details per each round
                        RoundDesc.setText(String.valueOf(matchRound));
                        TimeDesc.setText(String.valueOf(rd.timeLeftInRound));
                        LettersDesc.setText(letters);

                        //this will set the winner of each round
                        if (matchRound > 1 && gameStatus.hasGameEnded != true){
                            winnerDesc.setText("The winner of round " + (matchRound - 1) + " is " + serverImpl.getWinnerOfRound((matchRound - 1),gameId));
                        }

                        sleep(500);
                        //if the game has finally ended then we break the loop and end the thread
                        //while the game status is FALSE then we keep breaking this loop
                        if (!gameStatus.hasGameEnded){
                            System.out.println("Game Status: " + gameStatus.hasGameEnded);
                            break;
                        }else {
                            isOver = true;
                            break;
                        }
                    }

                    if (isOver == true){
                        System.out.println("Second loop is now stopping");
                        break;
                    }

                }
                System.out.println("##############");

                System.out.println("[CLIENT] Thread for timer is ending");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }
    }



    public static boolean checkGuessLength(String guess){
        if (minWordLength > guess.length()) {
            return false;
        }else if ( maxWordLength < guess.length()){
            return false;
        } else {
            return true;
        }
    }

    //checkWordWithCriteria
    public static boolean checkGuessLetters(StringBuilder letters, String guess) {
        Map<Character, Integer> letterCounts = new HashMap<>();
        for (int i = 0; i < letters.length(); i++) {
            char c = letters.charAt(i);
            letterCounts.put(c, letterCounts.getOrDefault(c, 0) + 1);
        }

        //If the guess does not conform to the letters given
        for (char c : guess.toCharArray()) {
            if (!letterCounts.containsKey(c) || letterCounts.get(c) <= 0) {
                return false;
            }
            letterCounts.put(c, letterCounts.get(c) - 1);
        }

        return true;
    }


}


