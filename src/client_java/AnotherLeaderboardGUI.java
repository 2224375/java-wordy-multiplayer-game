package client_java;

import WordyApp.TopFivePlayers;
import WordyApp.TopFiveWords;
import WordyApp.Wordy;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * This class is used to create a GUI for the leaderboard
 */
public class AnotherLeaderboardGUI {
    private JFrame frame = new JFrame();
    private JLabel top5PlayersLabel, top5WordsLabel, backgroundImageLabel;
    private JButton backButton;
    private JTable playerTable, wordTable;
    private ImageIcon icon;
    static Wordy serverImpl;
    String userName;
    int userId;

    /**
     * Contructor for initializing instance variables
     * @param serv
     * @param userName
     */
    public AnotherLeaderboardGUI(Wordy serv, String userName,int id) {
        serverImpl = serv;
        this.userName = userName;
        this.userId = id;

        frame.setSize(900,600);
        frame.setTitle("Wordy - A GameRoom");
        frame.setLocationRelativeTo(null);
        frame.setLayout(null);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            serverImpl.logOut(userName);
        }));

        // Window icon
        icon = new ImageIcon("src/client_java/logo-no-background.png");
        frame.setIconImage(icon.getImage());

        // Background image label
        backgroundImageLabel = new JLabel("", new ImageIcon("src/client_java/background-2.gif"),
                JLabel.CENTER);
        backgroundImageLabel.setBounds(-50, -70, 864, 864);

        // Top 5 playes label
        top5PlayersLabel = new JLabel("Top 5 Players with most wins");
        top5PlayersLabel.setFont(new Font("Arial", Font.BOLD, 18));
        top5PlayersLabel.setBounds(150, 66, 337, 30);

        // Top 5 words label
        top5WordsLabel = new JLabel("Top 5 Words ");
        top5WordsLabel.setFont(new Font("Arial", Font.BOLD, 18));
        top5WordsLabel.setBounds(150, 225, 337, 30);

        // Back button
        backButton = new JButton("Back");
        backButton.setBounds(50, 400, 165, 30);
        backButton.addActionListener(this::backButtonClicked);

        // top 5 players table
        DefaultTableModel playerModel = new DefaultTableModel(new String[]{"Rank","Player", "Total Wins"}, 0);
        playerTable = new JTable(playerModel);
        JScrollPane playerScrollPane = new JScrollPane(playerTable);
        playerScrollPane.setBounds(150, 100, 300, 100);

        // top 5 words table
        DefaultTableModel wordModel = new DefaultTableModel(new String[]{"Rank","Player", "Word"}, 0);
        wordTable = new JTable(wordModel);
        JScrollPane wordScrollPane = new JScrollPane(wordTable);
        wordScrollPane.setBounds(150, 255, 300, 100);

        // Adding of components
        frame.add(top5PlayersLabel);
        frame.add(top5WordsLabel);
        frame.add(backButton);
        frame.add(playerScrollPane);
        frame.add(wordScrollPane);
        frame.setLocationRelativeTo(null);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        // Fetch top players and words from the server and populate the tables
        try {
            TopFivePlayers[] topPlayers = serverImpl.getTopPlayers();
            populatePlayerTable(playerModel, topPlayers);

            TopFiveWords[] topWords = serverImpl.getTopWords();
            populateWordTable(wordModel, topWords);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Populates the player table with the top players
     * @param model
     * @param topPlayers Array of top players
     */
    private void populatePlayerTable(DefaultTableModel model, TopFivePlayers[] topPlayers) {
        for (int i = 0; i < topPlayers.length; i++) {
            TopFivePlayers player = topPlayers[i];
            Object[] row = {player.rank, player.username, player.winAmt};
            model.addRow(row);
        }
    }

    /**
     * Populates the word table with the top words
     * @param model
     * @param topWords Array of top words
     */
    private void populateWordTable(DefaultTableModel model, TopFiveWords[] topWords) {
        for (int i = 0; i < topWords.length; i++) {
            TopFiveWords word = topWords[i];
            Object[] row = {word.rank, word.username, word.word};
            model.addRow(row);
        }
    }

    /**
     * Shows the menu GUI when the back button is clicked
     * @param e Action performed
     */
    public void backButtonClicked(ActionEvent e) {
        if (e.getSource() == backButton) {
            frame.dispose();
            new AnotherMenuGUI(serverImpl, userName,userId);
        }
    }
}