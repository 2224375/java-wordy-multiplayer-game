package WordyApp;

/**
* WordyApp/NotEnoughPlayersExceptionHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Monday, June 26, 2023 8:52:25 PM CST
*/


//called when not enough players joined a game
public final class NotEnoughPlayersExceptionHolder implements org.omg.CORBA.portable.Streamable
{
  public WordyApp.NotEnoughPlayersException value = null;

  public NotEnoughPlayersExceptionHolder ()
  {
  }

  public NotEnoughPlayersExceptionHolder (WordyApp.NotEnoughPlayersException initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = WordyApp.NotEnoughPlayersExceptionHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    WordyApp.NotEnoughPlayersExceptionHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return WordyApp.NotEnoughPlayersExceptionHelper.type ();
  }

}
