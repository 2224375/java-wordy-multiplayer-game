package WordyApp;


/**
* WordyApp/TopFivePlayers.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Monday, June 26, 2023 8:52:25 PM CST
*/

public final class TopFivePlayers implements org.omg.CORBA.portable.IDLEntity
{
  public int rank = (int)0;
  public String username = null;
  public int winAmt = (int)0;

  public TopFivePlayers ()
  {
  } // ctor

  public TopFivePlayers (int _rank, String _username, int _winAmt)
  {
    rank = _rank;
    username = _username;
    winAmt = _winAmt;
  } // ctor

} // class TopFivePlayers
