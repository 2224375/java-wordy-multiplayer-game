package WordyApp;

/**
* WordyApp/GuessLengthExceptionHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Monday, June 26, 2023 8:52:25 PM CST
*/


//exception is called when the guess that a player made is too little or too many in character
public final class GuessLengthExceptionHolder implements org.omg.CORBA.portable.Streamable
{
  public WordyApp.GuessLengthException value = null;

  public GuessLengthExceptionHolder ()
  {
  }

  public GuessLengthExceptionHolder (WordyApp.GuessLengthException initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = WordyApp.GuessLengthExceptionHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    WordyApp.GuessLengthExceptionHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return WordyApp.GuessLengthExceptionHelper.type ();
  }

}
