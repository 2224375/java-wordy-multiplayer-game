package WordyApp;


/**
* WordyApp/TopFivePlayersHelper.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Wordy.idl
* Monday, June 26, 2023 8:52:25 PM CST
*/

abstract public class TopFivePlayersHelper
{
  private static String  _id = "IDL:WordyApp/TopFivePlayers:1.0";

  public static void insert (org.omg.CORBA.Any a, WordyApp.TopFivePlayers that)
  {
    org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
    a.type (type ());
    write (out, that);
    a.read_value (out.create_input_stream (), type ());
  }

  public static WordyApp.TopFivePlayers extract (org.omg.CORBA.Any a)
  {
    return read (a.create_input_stream ());
  }

  private static org.omg.CORBA.TypeCode __typeCode = null;
  private static boolean __active = false;
  synchronized public static org.omg.CORBA.TypeCode type ()
  {
    if (__typeCode == null)
    {
      synchronized (org.omg.CORBA.TypeCode.class)
      {
        if (__typeCode == null)
        {
          if (__active)
          {
            return org.omg.CORBA.ORB.init().create_recursive_tc ( _id );
          }
          __active = true;
          org.omg.CORBA.StructMember[] _members0 = new org.omg.CORBA.StructMember [3];
          org.omg.CORBA.TypeCode _tcOf_members0 = null;
          _tcOf_members0 = org.omg.CORBA.ORB.init ().get_primitive_tc (org.omg.CORBA.TCKind.tk_long);
          _members0[0] = new org.omg.CORBA.StructMember (
            "rank",
            _tcOf_members0,
            null);
          _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
          _members0[1] = new org.omg.CORBA.StructMember (
            "username",
            _tcOf_members0,
            null);
          _tcOf_members0 = org.omg.CORBA.ORB.init ().get_primitive_tc (org.omg.CORBA.TCKind.tk_long);
          _members0[2] = new org.omg.CORBA.StructMember (
            "winAmt",
            _tcOf_members0,
            null);
          __typeCode = org.omg.CORBA.ORB.init ().create_struct_tc (WordyApp.TopFivePlayersHelper.id (), "TopFivePlayers", _members0);
          __active = false;
        }
      }
    }
    return __typeCode;
  }

  public static String id ()
  {
    return _id;
  }

  public static WordyApp.TopFivePlayers read (org.omg.CORBA.portable.InputStream istream)
  {
    WordyApp.TopFivePlayers value = new WordyApp.TopFivePlayers ();
    value.rank = istream.read_long ();
    value.username = istream.read_string ();
    value.winAmt = istream.read_long ();
    return value;
  }

  public static void write (org.omg.CORBA.portable.OutputStream ostream, WordyApp.TopFivePlayers value)
  {
    if (value != null){
      ostream.write_long (value.rank);
      ostream.write_string (value.username);
      ostream.write_long (value.winAmt);
    }else {
      ostream.write_long(0);
      ostream.write_string ("N/A");
      ostream.write_long(0);
    }

/*
   ostream.write_long (value.rank);
    ostream.write_string (value.username);
    ostream.write_long (value.winAmt);
 */

  }

}
